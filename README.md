#Kudu Engine

Kudu is a game engine written in python (compatible with 2-3), depending on python-sfml and Box2D. It can be embed in [Pookoo](https://bitbucket.org/efarhan/pookoo) engine.

It use JSON to store game data like the structure of level and the images of the player animation, or the GUI.

Documentation [here](http://team-kwakwa.com/kudu_doc/index.html)

###Install
Install [python-sfml](http://www.python-sfml.org/)
and [Box2D](http://code.google.com/p/pybox2d/)

### Create game project
- Create an init JSON file in data/json/init.json who will look like this:
``` {
	"init": "data/json/level.json",
	"screen_size": [1280,720]
} ```

- Create a level JSON file:
``` {
	"images": {
	},
	"physic_object": {
	}
} ```


###TODO
- Editor
- GUI element
- Custom init creation at start

###License
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2014 Elias Farhan <elias.farhan@team-kwakwa.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.