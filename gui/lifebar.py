from engine import img_manager
from engine.rect import Rect
from engine.vector import Vector2
from game_object.game_object_main import GameObject

__author__ = 'Elias'


class LifeBar(GameObject):
    def __init__(self):
        GameObject.__init__(self)

    def loop(self,screen):
        img_manager.draw_rect(screen,Vector2(),Rect(self.pos,self.size),)
