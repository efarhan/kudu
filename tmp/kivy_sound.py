from kivy.core.audio import Sound, SoundLoader

__author__ = 'Elias'

from kivy.clock import Clock
from kivy.core.window import Window
from kivy.graphics.context_instructions import Rotate, Color, PushMatrix, PopMatrix
from kivy.graphics.vertex_instructions import Rectangle
from kivy.uix.image import Image
from kivy.uix.widget import Widget

__author__ = 'Elias'

import kivy

import kivy
kivy.require('1.0.6') # replace with your current kivy version !

from kivy.app import App
from kivy.uix.label import Label

class KuduGame(Widget):

    def __init__(self,**kwargs):
        super(KuduGame, self).__init__(**kwargs)
        folder_name = "../data/sprites/hero/move/"

        sound = SoundLoader.load('../data/sound/pissed_off_duck.wav')
        if sound:
            print("Sound found at %s" % sound.source)
            print("Sound is %.3f seconds" % sound.length)
            sound.play()
        self.counter = 0
        self.anim_index = 0

    def update(self, delta_time):
        pass


        self.counter += 1


class KuduApp(App):

    def build(self):
        from kivy.base import EventLoop
        EventLoop.ensure_window()
        self.window = EventLoop.window
        self.kudu_widget = KuduGame(app=self)
        self.root = self.kudu_widget
        kivy_screen = self.kudu_widget
        Clock.schedule_interval(self.kudu_widget.update, 1.0 / 60.0)


if __name__ == '__main__':
    KuduApp().run()