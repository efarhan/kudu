from kivy.clock import Clock
from kivy.core.window import Window
from kivy.graphics.context_instructions import Rotate, Color, PushMatrix, PopMatrix
from kivy.graphics.vertex_instructions import Rectangle
from kivy.uix.image import Image
from kivy.uix.widget import Widget

__author__ = 'Elias'

import kivy

import kivy
kivy.require('1.0.6') # replace with your current kivy version !

from kivy.app import App
from kivy.uix.label import Label

class KuduGame(Widget):

    def __init__(self,**kwargs):
        super(KuduGame, self).__init__(**kwargs)
        folder_name = "../data/sprites/hero/move/"
        self.animation = [
            folder_name+"hero1.png",
            folder_name+"hero2.png",
            folder_name+"hero3.png"
        ]
        self.imgs = []
        for filename in self.animation:
            self.imgs.append(Image(source=filename))
        self.counter = 0
        self.anim_index = 0

    def update(self, delta_time):
        self.clear_widgets()
        if self.counter %20 == 0:

            self.anim_index = (self.anim_index + 1)%len(self.imgs)

        self.add_widget(self.imgs[self.anim_index])


        for i in range(0,5):
            new_rectangle = Widget()
            new_rectangle.pos = (i*200,Window.size[1]/2)
            new_rectangle.size = (200,100)
            with new_rectangle.canvas:
                PushMatrix()
                Color(1.0,1.0,1.0,1.0)
                Rotate(angle=-self.counter,origin=new_rectangle.center)

                Rectangle(pos=new_rectangle.pos,
                          size=new_rectangle.size)
                PopMatrix()

            self.add_widget(new_rectangle)


        self.counter += 1


class KuduApp(App):

    def build(self):
        from kivy.base import EventLoop
        EventLoop.ensure_window()
        self.window = EventLoop.window
        self.kudu_widget = KuduGame(app=self)
        self.root = self.kudu_widget
        kivy_screen = self.kudu_widget
        Clock.schedule_interval(self.kudu_widget.update, 1.0 / 60.0)


if __name__ == '__main__':
    KuduApp().run()