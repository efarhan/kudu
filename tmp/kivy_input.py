__author__ = 'Elias'


from kivy.clock import Clock
from kivy.core.window import Window
from kivy.uix.image import Image
from kivy.uix.widget import Widget

__author__ = 'Elias'

import kivy

import kivy
kivy.require('1.0.6') # replace with your current kivy version !

from kivy.app import App
from kivy.uix.label import Label

class KuduGame(Widget):

    def __init__(self,**kwargs):
        super(KuduGame, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._keyboard.bind(on_key_up=self._on_keyboard_up)


    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard.unbind(on_key_up=self._on_keyboard_up)
        self._keyboard = None

    def _on_keyboard_down(self, *args):
        print args[1][0]

    def _on_keyboard_up(self, *args):
        pass
    def update(self, delta_time):

        pass

class KuduApp(App):

    def build(self):
        from kivy.base import EventLoop
        EventLoop.ensure_window()
        self.window = EventLoop.window
        self.kudu_widget = KuduGame(app=self)
        self.root = self.kudu_widget
        kivy_screen = self.kudu_widget
        Clock.schedule_interval(self.kudu_widget.update, 1.0 / 60.0)


if __name__ == '__main__':
    KuduApp().run()