#!usr/bin/python
import pyglet
import os
window = pyglet.window.Window()
counter=.0
working_dir = os.path.dirname(os.path.realpath(__file__))
pyglet.resource.path = [os.path.join(working_dir,'..')]
pyglet.resource.reindex()
path = "data/sprites/hero/move/"


def load_anim():
    arrImages=[]
    for i in range(1,4):
        tmpImg=pyglet.resource.image(path+"hero"+str(i)+".png")
        arrImages.append(tmpImg)
    return arrImages


def update_frames(dt):
    global counter
    counter=(counter+1.0/6)%3


@window.event
def on_draw():
    pyglet.gl.glClearColor(0,0,0,0)
    window.clear()
    frames[int(counter)].blit(320,200,0,
                              frames[int(counter)].width,
                              frames[int(counter)].height)

frames = load_anim()
pyglet.clock.schedule_interval(update_frames,1/60.0)
pyglet.app.run()