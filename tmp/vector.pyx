from libc.math cimport sqrt, sin, cos, M_PI
from numbers import Number
from engine.const import log

cdef class Matrix22:
    def __init__(self, values):
        self.values = values

    cpdef mult(self, Vector2 vector):
        return  (self.values[0][0]*vector.x+self.values[0][1]*vector.y,
                self.values[1][0]*vector.x+self.values[1][1]*vector.y)

cdef class Vector2:
    cdef double x, y, length

    def __init__(self,*args):
        if args is None:
            log("Error: giving None as Vector2 arg",1)
            return
        try:
            self.x = 0.0
            self.y = 0.0
            self.length = 0.0
            if len(args) == 2:
                self.x = args[0]
                self.y = args[1]
            elif len(args) == 1:
                if args[0] is None:
                    log("Error: giving None as Vector2 arg",1)
                    return
                if not isinstance(args[0], Vector2):
                    self.x = args[0][0]
                    self.y = args[0][1]
                else:
                    self.x = args[0].x
                    self.y = args[0].y
            self.length = sqrt(self.x**2+self.y**2)
        except IndexError as e:
            log("Error: Vector2 with argument: "+str(args)+ "|"+str(e), 1)

        except AttributeError as e:
            log("Error: Vector2 with attribute error: "+str(args)+ "|"+str(e), 1)


    @staticmethod
    def orientation(double length, double angle):
        x = length*cos(angle*M_PI/180)
        y = length*sin(angle*M_PI/180)
        return Vector2(x,y)

    cpdef normalize(self):
        cdef double ratio
        try:
            ratio = 1.0/self.length
            x = self.x*ratio
            y = self.y*ratio
            return Vector2(x,y)
        except ZeroDivisionError:
            return Vector2()

    cpdef invert(self):
        return Vector2(self.y, self.x)

    cpdef rotate(self, angle):
        (self.x, self.y) = Matrix22([
            [cos(angle*M_PI/180),-sin(angle*M_PI/180)],
            [sin(angle*M_PI/180),cos(angle*M_PI/180)]]) * self

    cpdef add(self, v):
        return Vector2(self.x+v.x, self.y+v.y)

    cpdef sub(self,v):
        try:
            return Vector2(self.x-v.x, self.y-v.y)
        except AttributeError:
            raise AttributeError(str(v[0]))

    cpdef mult(self,v):
        """Warning multiply (x,y) * (x',y') = (x*x', y*y')"""
        if v.__class__ == Vector2:
            return Vector2(self.x*v.x, self.y*v.y)
        elif isinstance(v, Number):
            return Vector2(self.x*v, self.y*v)
        else:
            raise TypeError("Vector can only multiply numbers or Vector, type given: %s"%(str(type(v))))


    cpdef div(self,v):
        try:
            if v.__class__ == Vector2:
                return Vector2(float(self.x)/v.x, float(self.y)/v.y)
            elif isinstance(v, Number):
                return Vector2(self.x/float(v), self.y/float(v))
            else:
                raise TypeError("Vector can only divide numbers or Vector, type given: %s"%(str(type(v))))

        except ZeroDivisionError:
            log("Error: Division by zero with: "+str(self.get_tuple())+", "+str(v.get_tuple()),1)
            return None

    cpdef int_div(self,v):
        try:
            if v.__class__ == Vector2:
                return Vector2(self.x/v.x, self.y/v.y)
            elif isinstance(v, Number):
                return Vector2(self.x/v, self.y/v)
            else:
                raise TypeError("Vector can only divide numbers or Vector, type given: %s"%(str(type(v))))
        except ZeroDivisionError:
            log("Error: Division by zero with: "+str(self.get_tuple())+", "+str(v.get_tuple()),1)
            return None

    cpdef dot(self,v):
        """Dot product"""
        return self.x*v.x+self.y*v.y

    cpdef get_tuple(self):
        return self.x,self.y

    cpdef get_list(self):
        return [self.x,self.y]

    cpdef get_int_tuple(self):
        return int(self.x),int(self.y)

    cpdef get_float_tuple(self):
        return float(self.x),float(self.y)

    cpdef get_string(self):
        return "("+str(self.x)+","+str(self.y)+")"

    cpdef get_ratio(self):
        return float(self.x)/self.y

    def __repr__(self):
        return str(self.get_tuple())

    def __add__(self, v):
        return self.add(v)

    def __sub__(self, v):
        return self.sub(v)

    def __mul__(self, v):
        return self.mult(v)

    def __div__(self, v):
        return self.div(v)

    def __truediv__(self, v):
        return self.int_div(v)



cdef class IntVector2:
    cdef int x, y
    cdef double length