"""Abstract Render Engine template"""
import sys
import traceback
from engine import level_manager, physics_manager
from engine.const import log, CONST
from engine.input_manager import input_manager
from engine.vector import Vector2
from engine.physics_manager import physics_manager


class Engine():
    """Abstract Engine class"""
    def __init__(self):
        self.screen = None
        self.finish = False
        self.real_screen_size = Vector2()
        self.screen_diff_ratio = Vector2()
        self.screen_size = Vector2(CONST.screen_size)
        self.origin_pos = Vector2()
        self.ratio = 1.0
        self.delta_time = 0.03

    def get_framerate(self):
        return 1.0/(self.delta_time/10.0**6)

    def on_resize(self, new_size):
        self.real_screen_size = new_size
        Engine.init_screen(self)

    def init_screen(self):
        self.screen_diff_ratio = self.real_screen_size / self.screen_size
        self.update_origin_pos()
        self.update_ratio()

    def init_joystick(self):
        pass


    @staticmethod
    def init_level():
        """if CONST.debug:
            from scenes.logo_kwakwa import Kwakwa
            level_manager.switch_level(Kwakwa())
        else:"""

        if not isinstance(CONST.scene,CONST.string_type):
            log("Error: Unvalid string type: "+str(CONST.scene),1)
            return
        for c in CONST.scene:
            if c != '.' and c != '_' and not c.isalpha():
                log("Error: Unvalid Character: "+str(CONST.scene),1)
                return
        dir_list = CONST.scene.split(".")
        try:
            exec('''from %s import %s'''%(".".join(dir_list[0:len(dir_list)-1]), dir_list[len(dir_list)-1]))
        except ImportError:
            log("Error: ImportError with: "+str(CONST.scene),1)
            return
        try:
            exec('''level_manager.switch_level(%s())'''%(dir_list[len(dir_list)-1]))
        except Exception as e:
            log('Error with loading scenes: %s'%(CONST.scene)+str(e),1)
            traceback.print_exc(file=sys.stderr)
            return


    def init_all(self):
        self.init_screen()
        self.init_joystick()
        input_manager.init_actions()
        self.init_level()

    def loop(self):
        if sys.platform == 'darwin':
            input_manager.add_button('quit', ['LSYS+q'])
        else:
            input_manager.add_button('quit', ['LCTRL+q'])
        input_manager.add_button('reset', ['r'])
        while not self.finish:
            self.pre_update()


            self.finish = input_manager.get_button('quit')
            engine.update_event()
            f = level_manager.update_level()

            if f is None:

                log("No scenes loaded",1)
                break
            else:
                f(self.screen)
            """if input_manager.get_button('reset'):
                from scenes.gamestate import GameState
                level_manager.switch_level(GameState(CONST.startup))"""
            self.post_update()
        self.exit()
    def pre_update(self):
        pass

    def post_update(self):
        pass

    def exit(self):
        physics_manager.exit()

    def get_screen_size(self):
        return self.screen_size

    def get_origin_pos(self):
        return self.origin_pos

    def get_ratio(self):
        return self.ratio

    def update_ratio(self):
        if self.real_screen_size.get_ratio() >= self.screen_size.get_ratio():
            screen_diff_ratio = self.screen_diff_ratio.y

        else:
            screen_diff_ratio = self.screen_diff_ratio.x

        self.ratio = screen_diff_ratio
        log("RATIO:"+str(self.ratio))
        log("SCREEN_DIFF_RATIO:"+str(self.screen_diff_ratio))
        log("SCREEN_SIZE:"+str(self.screen_size))
        log("REAL SCREEN SIZE:"+str(self.real_screen_size))
        log("FIRST RECT SIZE:"+str((self.origin_pos.x, self.real_screen_size.y))+" OR "+str((self.real_screen_size.x, self.origin_pos.y)))

    def update_origin_pos(self):
        if self.real_screen_size.get_ratio() >= self.screen_size.get_ratio():

            origin_pos = Vector2((self.real_screen_size.x -
                                  self.real_screen_size.y *
                                  self.screen_size.get_ratio()) / 2, 0)
        else:
            origin_pos = Vector2(0, (self.real_screen_size.y -
                                     self.real_screen_size.x /
                                     self.screen_size.get_ratio()) / 2)
        self.origin_pos = origin_pos
        log("ORIGIN_POS:" +str(self.origin_pos))

    def update_event(self):
        pass

    def set_framerate(self, framerate):
        pass

engine = Engine()

real_screen_size = Vector2()
kivy_screen = None
if CONST.render == 'sfml':
    log("Creating SFMLEngine")
    from sfml_engine.sfml_init import SFMLEngine
    engine = SFMLEngine()
    log("Engine: "+str(engine))
elif CONST.render == 'pookoo':
    log("Creating PookooEngine")
    from pookoo_engine.pookoo_init import PookooEngine
    engine = PookooEngine()

elif CONST.render == 'kivy':
    log("Creating KivyEngine")
    from tmp.init.kivy_init import KivyEngine
    engine = KivyEngine()
    log(engine)
elif CONST.render == 'pyglet':
    from tmp.init.pyglet_init import PyGLETEngine
    engine = PyGLETEngine()
    log(engine)
"""
elif CONST.render == 'pookoo': \
elif CONST.render == 'kivy':
    import kivy
    from kivy.app import App
    from kivy.clock import Clock
    from kivy.core.window import Window
    from kivy.config import Config
    from kivy.uix.widget import Widget
    from input.keyboard_input import _on_keyboard_down

    class KuduGame(Widget):

        def __init__(self,**kwargs):
            super(KuduGame, self).__init__(**kwargs)
            self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
            self._keyboard.bind(on_key_down=_on_keyboard_down)

        def _keyboard_closed(self):
            self._keyboard.unbind(on_key_down=_on_keyboard_down)
            self._keyboard = None

        def update(self, delta_time):
            get_level().loop(self)

    class KuduApp(App):
        def build(self):
            # print the application informations
            log('\nKudu v%s  Copyright (C) 2014  Elias Farhan')

            from kivy.base import EventLoop
            EventLoop.ensure_window()
            self.window = EventLoop.window


            global real_screen_size, kivy_screen
            self.kudu_widget = KuduGame(app=self)
            self.root = self.kudu_widget

            kivy_screen = self.kudu_widget

            from engine.loop import init_level
            init_level()
            Clock.schedule_interval(self.kudu_widget.update, 1.0 / 60.0)
    """


'''
def init_all():
    global kivy_screen
    init_engine.init_screen()
    if CONST.render == 'sfml':
        log(str(screen.settings))
    init_joystick()
    init_actions()
    return init_engine.screen





def init_screen():
    screen_size = CONST.screen_size
    log("Init screen with screen_size:" + str(screen_size))
    if CONST.render == 'sfml':

        return window
    elif CONST.render == 'pookoo':
        return pookoo.window.begin()
    elif CONST.render == 'kivy':

        return KuduApp()


def init_joystick():
    pass


def get_screen_size(relative=False):
    if not relative:
        return Vector2(CONST.screen_size[0], CONST.screen_size[1])
    else:
        if CONST.render == 'pookoo':
            return Vector2(pookoo.window.size())


def toogle_fullscreen():
    """TODO: toggle fullscreen"""


def resize_screen(new_size):
    """TODO: resize window"""

def get_kivy_screen():
    global kivy_screen
    return kivy_screen
'''