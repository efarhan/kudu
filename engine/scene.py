from engine.img_manager import img_manager
from engine.vector import Vector2


class Scene():
    def __init__(self):
        self.screen_pos = Vector2()

    def init(self, loading=False):
        pass

    def loop(self, screen):
        pass

    def exit(self):
        img_manager.sanitize_img_manager()