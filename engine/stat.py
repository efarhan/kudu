"""
Stat are global values stored during the execution of the Engine
"""

from engine.const import log
values = {}

def egal_condition(name,v):
    try:
        return get_value(name) == v
    except KeyError:
        return False
    
def greater_condition(name,v):
    try:
        return get_value(name) > v
    except KeyError:
        return False
    
def lesser_condition(name,v):
    try:
        return get_value(name) < v
    except KeyError:
        return False

def set_value(name,v):
    values[name] = v

def get_value(name):
    try:
        return values[name]
    except KeyError:
        return None
    
