"""
Abstraction of the loop function
providing the level
"""

from engine.const import CONST


level = None


def switch_level(level_obj):
    """Switch to a other Scene"""
    global level
    if level is not None:
        level.exit()
    level = level_obj
    if level is not None:
        CONST.parse_const(CONST.path_prefix+"data/json/init.json")
        level.init()
    else:
        from engine.init import engine
        engine.finish = True


def update_level():
    global level
    if level is None:

        return level
    return level.loop


def get_level():
    global level
    return level