"""
Manage input like joystick, keyboard and mouse
"""
from engine.const import CONST, log
from json_export.json_main import load_json


__author__ = 'efarhan'


class InputManager():
    def __init__(self):
        self.button_key = {}
        self.button_map = {}
        self.button_value = {}

    def init(self):
        self.init_keyboard()
        self.init_joystick()

    def add_button(self,action, key_list):
        self.add_joy_button(action, key_list)
        self.add_key_button(action, key_list)

    def add_joy_button(self,action,key_list):
        pass

    def add_key_button(self,action,key_list):
        pass

    def get_joy_button(self,action):
        pass

    def get_button(self, action):
        return self.get_key_button(action) or self.get_joy_button(action)

    def init_keyboard(self):
        """button_map = {'action' : 'key_list'}"""
        self.button_map = {}

        '''button_value = {'key' : value}'''
        self.button_value = {}

        '''button_key = {'render_key': 'key'}'''
        self.button_key = {}

    def init_joystick(self):

        '''axis'''
        self.axis = {}

        self.hat = {}

    def get_current_key(self):
        current_keys = []
        for key in self.button_value.keys():
            if self.button_value[key]:
                current_keys.append(key)
        return current_keys

    def get_key_button(self, action):
        try:
            value = False
            for key in self.button_map[action]:
                if "+" in key:
                    key_value = True
                    for k in key.split("+"):
                        key_value = key_value and self.button_value[k]
                    value = (value or key_value)
                else:
                    value = (value or self.button_value[key])
            return value
        except KeyError:
            return False

    def update_button(self,key,value):
        pass

    def remove_action(self,action):
        del self.button_map[action]

    @staticmethod
    def init_actions():
        """Load control scheme from CONST.actions file"""
        if isinstance(CONST.actions, CONST.string_type):
            actions = load_json(CONST.actions)
            for key in actions.items():
                log(key)
                input_manager.add_button(key[0], key[1])
        else:
            log("Error: could not load actions, should ba a path to JSON file. It is: "+str(type(CONST.actions)), 1)
            return

    @staticmethod
    def clear_actions():
        if isinstance(CONST.actions, CONST.string_type):
            actions = load_json(CONST.actions)
            for key in actions.items():
                input_manager.remove_action(key[0])
        else:
            log("Error: could not load actions, should ba a path to JSON file. It is: "+str(type(CONST.actions)), 1)
            return

    @staticmethod
    def reset_actions():
        if isinstance(CONST.actions_bak, CONST.string_type):
            actions = load_json(CONST.actions_bak)
            for key in actions.items():
                input_manager.remove_action(key[0])
                input_manager.add_button(key[0], key[1])
        else:
            log("Error: could not load actions, should ba a path to JSON file. It is: "+str(type(CONST.actions)), 1)
            return

    @staticmethod
    def update_actions():
        if isinstance(CONST.actions, CONST.string_type):
            actions = load_json(CONST.actions)
            for key in actions.items():
                input_manager.remove_action(key[0])
                input_manager.add_button(key[0], key[1])
        else:
            log("Error: could not load actions, should ba a path to JSON file. It is: "+str(type(CONST.actions)), 1)
            return


input_manager = InputManager()

if CONST.render == 'sfml':
    from sfml_engine.sfml_input import SFMLInput
    input_manager = SFMLInput()
elif CONST.render == 'pookoo':
    from pookoo_engine.pookoo_input import PookooInput
    input_manager = PookooInput()
elif CONST.render == 'kivy':
    from engine.input_engine.kivy_input import KivyInputManager
    input_manager = KivyInputManager()
