'''
Manage the font loading and rendering

Created on Feb 19, 2014

@author: efarhan
'''
from engine.const import CONST, log


class FontManager:
    def __init__(self):
        self.fonts = {}

    def pixel2point(self,pixel):
        return int(pixel*3/4)

    def load_font(self,name,pixel_size):
        '''Use pixel size'''

        try:
            self.fonts[name]
        except KeyError:
            try:
                self.fonts[name] = self.import_font(name)
            except IOError:
                return None
        return self.fonts[name]

    def import_font(self,filename):
        pass

    def load_text(self,font,text,color=(0,0,0),pixel_height=0,old_txt=None):
        pass
    
    def show_text(self, text, screen, pos, angle=0, center=False, size=None, center_image=False,flip=False):
        pass

    def get_size(self,text):
        pass



font_manager = FontManager()
if CONST.render == 'sfml':

    from sfml_engine.sfml_font_manager import SFMLFontManager
    font_manager = SFMLFontManager()
elif CONST.render == 'pookoo':
    from pookoo_engine.pookoo_font_manager import PookooFontManager
    font_manager = PookooFontManager()
elif CONST.render == 'kivy':
    from engine.font_engine.kivy_font_manager import KivyFontManager
    font_manager = KivyFontManager()