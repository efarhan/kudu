from engine.const import CONST, log

if CONST.network == "python":
    import socket

__author__ = 'efarhan'


class PyNetworkManager():
    def __init__(self):
        self.sock = None
        self.is_connect = False

    def connect(self,host=CONST.host, port=CONST.port):
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((host,port))
        except Exception:
            log("Could not connect to host",1)
        else:
            log("Could connect to host")
            self.is_connect = True


    def send_data(self,data):
        if self.is_connect:
            try:
                self.sock.sendall(data)
            except socket.error:
                is_connect = False

    def recv_data(self):
        data = None
        try:
            data = self.sock.recv(1024).strip()
        except socket.error as e:
            log(e)
            self.is_connect = False
        return data

    def disconnect(self):
        self.sock.close()
        self.is_connect = False

network_manager = None
if CONST.network == "python":
    network_manager = PyNetworkManager()

elif CONST.network == "sfml":
    pass
elif CONST.network == "pookoo":
    pass