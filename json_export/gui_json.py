'''
Parse gui JSON file
'''
from engine.const import log, CONST
from json_export.json_main import load_json

def load_gui(level,filename):
    gui_data = load_json(filename)
    try:
        gui_type = gui_data["type"]
    except KeyError:
        log("Error: No gui_type for:" + str(gui_data),1)
        return
    
    if not isinstance(gui_type,CONST.string_type):
        return
    for c in gui_type:
        if c != '.' and c.isalpha():
            return
    dir_list = gui_type.split(".")
    try:
        exec('''from %s import %s'''%(".".join(dir_list[0:len(dir_list)-1]), dir_list[len(dir_list)-1]))
    except ImportError:
        log("Error: ImportError with: "+str(gui_type),1)
        return
    try:
        d = locals()
        exec('''gui = %s.parse_gui(gui_data)'''%(dir_list[len(dir_list)-1]),globals(),d)
        gui = d['gui']
    except Exception as e:
        log('Error with loading gui_type: %s'%(gui_type)+str(e),1)
        return
    return None