'''
Physics object parser
'''
import math

from engine.physics_manager import physics_manager
from engine.const import CONST
from json_export.json_main import get_element
from engine.physics_manager import BodyType
from engine.init import engine
from engine.vector import Vector2


def load_physic_objects(physics_data,image):
    if image is None:
        return
    body_type = get_element(physics_data, "type")
    angle = get_element(physics_data, "angle")
    if angle is None:
        angle = image.angle
    if body_type:
        pos = Vector2()
        if image.pos:
            pos = image.pos
        if image.screen_relative_pos:
            pos = pos+image.screen_relative_pos*engine.get_screen_size()

        if image.size:
            pos = pos+image.size/2
        if body_type == "dynamic":
            image.body = physics_manager.add_body(pos, BodyType.dynamic, angle)
        elif body_type == "static":
            image.body = physics_manager.add_body(pos, BodyType.static, angle)
        elif body_type == 'kinematic':
            image.body = physics_manager.add_body(pos, BodyType.kinematic, angle)
    pos = (0,0)
    if image.pos:
        pos = image.pos
    if image.screen_relative_pos:
        pos = pos+image.screen_relative_pos*engine.get_screen_size()+image.size/2

            
    fixtures_data = get_element(physics_data,"fixtures")
    if fixtures_data:
        for physic_object in fixtures_data:
            sensor = get_element(physic_object, "sensor")
            if sensor is None:
                sensor = False
            user_data = get_element(physic_object,"user_data")
            if user_data is None:
                user_data = image
            obj_type = get_element(physic_object, "type")
            if obj_type == "box":
                pos = get_element(physic_object,"pos")
                if not pos:
                    pos = Vector2()

                size = get_element(physic_object,"size")
                if not size:
                    size = image.size/2

                angle = get_element(physic_object,"angle")
                if angle is None:
                    
                    angle = 0
                image.fixtures.append(physics_manager.add_box(image.body, Vector2(pos), Vector2(size), angle, user_data, sensor))
            elif obj_type == "circle":
                pos = get_element(physic_object,"pos")
                if not pos:
                    pos = (0,0)
                radius = get_element(physic_object,"radius")
                if not radius:
                    radius = 1

                image.fixtures.append(physics_manager.add_circle(image.body,pos,radius,sensor,user_data))
