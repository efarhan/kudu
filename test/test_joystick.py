from engine.const import log
from engine.img_manager import img_manager
from engine.init import engine
from engine.input_manager import input_manager
from engine.rect import Rect
from engine.scene import Scene
from engine.vector import Vector2
from game_object.text import Text

__author__ = 'Elias Farhan'


class TestJoystick(Scene):
    def __init__(self):
        Scene.__init__(self)
        self.label = []
        self.values = []
        self.char_size = 50
        self.max_size = 0

    def init(self, loading=False):
        Scene.init(self)

        for i,axis in enumerate(input_manager.axis.keys()):
            text = Text(pos=Vector2(0,i*self.char_size),
                                   size=self.char_size,
                                   font="data/font/pixel_arial.ttf",
                                   text=axis,
                                   color=(255,255,255))
            if text.size.x > self.max_size:
                self.max_size = text.size.x
            self.label.append(text)
        for i, label in enumerate(self.label):
            text = Text(pos=Vector2(self.max_size+10,i*self.char_size),
                                   size=self.char_size,
                                   font="data/font/pixel_arial.ttf",
                                   text="0",
                                   color=(255,255,255))
            self.values.append(text)


    def loop(self, screen):
        Scene.loop(self,screen)
        img_manager.draw_rect(screen,
                              Rect(Vector2(),engine.screen_size),
                              color=(0,0,0))
        for l in self.label:
            l.loop(screen
            )
        for i, v in enumerate(self.values):
            v.set_text(str(input_manager.axis[self.label[i].text]))
            v.loop(screen)
