from engine.scene import Scene
from engine.network_manager import network_manager
from engine.const import log

__author__ = 'efarhan'


class NetworkTest(Scene):
    def __init__(self):
        Scene.__init__(self)
        network_manager.connect()

    def init(self, loading=False):
        Scene.init(self,loading=False)

    def loop(self, screen):
        Scene.loop(self, screen)
        network_manager.send_data("Hello world!")
        log(network_manager.recv_data())