from animation.animation_main import Animation
from engine.scene import Scene
from engine.vector import Vector2
from game_object.image import Image
from engine.img_manager import img_manager
from engine.rect import Rect
from engine.const import CONST, log
from engine.init import engine

__author__ = 'Elias'


class ImageTest(Scene):
    def __init__(self):
        Scene.__init__(self)
        self.nmb = Vector2(10,10)
        self.images = []
        self.img_size = Vector2(64,64)

        self.nmb = engine.screen_size/self.img_size

    def init(self, loading=False):
        for i in range(int(self.nmb.x)):
            for j in range(int(self.nmb.y)):
                img_tmp = Image(pos=Vector2(i*self.img_size.x, j*self.img_size.y ),size=self.img_size)
                img_tmp.anim = Animation(img_tmp)
                img_tmp.anim.root_path = "data/sprites/hero/"
                img_tmp.anim.path_list = ["move/"]
                img_tmp.anim.state_range = {"move": [0,3]}
                img_tmp.anim.state = "move"
                img_tmp.init_image()

                self.images.append(img_tmp)

    def loop(self, screen):
        img_manager.draw_rect(screen, rect=Rect(Vector2(),size=engine.screen_size),color=[255,255,255])
        
        log(engine.get_framerate())
        for img in self.images:
            img.loop(screen)