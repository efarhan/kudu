import time
import datetime
import sfml

__author__ = 'Elias'

import unittest
from engine.vector import Vector2

class TestPureVector(unittest.TestCase):
    def setUp(self):
        self.begin = time.clock()
        self.iteration = 1000

    def test_initialize(self):
        for i in range(self.iteration):
            new_vector = Vector2([2,1])
            self.assertEqual(new_vector.get_int_tuple(), (2,1))
            new_vector = Vector2((2,1))
            self.assertEqual(new_vector.get_int_tuple(), (2,1))
            new_vector = Vector2(sfml.Vector2(2,1))
            self.assertEqual(new_vector.get_int_tuple(), (2,1))

    def test_addition(self):
        for i in range(self.iteration):
            new_vector = Vector2() + Vector2(2,1)
            self.assertEqual(new_vector.get_int_tuple(), (2,1))

    def test_subtract(self):
        for i in range(self.iteration):
            new_vector = Vector2() - Vector2(2,1)
            self.assertEqual(new_vector.get_int_tuple(), (-2,-1))

    def test_multiply(self):
        for i in range(self.iteration):
            new_vector = Vector2(1,1) * Vector2(2,1)
            self.assertEqual(new_vector.get_int_tuple(),(2,1))

    def test_division(self):
        for i in range(self.iteration):
            new_vector = Vector2(2,1) / Vector2(2,1)
            self.assertEqual(new_vector.get_int_tuple(),(1,1))

    def test_factor_multiply(self):
        for i in range(self.iteration):
            new_vector = Vector2(2,1)*2
            self.assertEqual(new_vector.get_int_tuple(),(4,2))

    def test_rotate(self):
        for i in range(self.iteration):
            new_vector = Vector2(0,1)
            new_vector.rotate(-90)
            self.assertEqual(new_vector.get_int_tuple(),(1,0))

    def test_normalize(self):
        for i in range(self.iteration):
            new_vector = Vector2(3,4).normalize()
            self.assertEqual(int(new_vector.length), 1)

    def test_orientation(self):
        for i in range(self.iteration):
            new_vector = Vector2.orientation(1,0)
            self.assertEqual(new_vector.get_int_tuple(),(1,0))

    def tearDown(self):
        print "%.2gs"%(time.clock()-self.begin)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPureVector)
    unittest.TextTestRunner(verbosity=1).run(suite)

