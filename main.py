#/usr/bin/env python
import sys
import os
sys.path.append(os.path.abspath("."))
from engine.init import engine


if __name__ == "__main__" or __name__ == "main":
    engine.init_all()
    engine.loop()
