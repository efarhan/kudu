'''
Created on 20 janv. 2015

@author: efarhan
'''
from game_object.game_object_main import GameObject
from game_object.image import Image
from engine.img_manager import img_manager, BLEND_MODE
from engine.rect import Rect
from engine.vector import Vector2
from engine.const import CONST, log
from json_export.json_main import get_element

class MaskImage(GameObject):

    def __init__(self, image, mask):
        GameObject.__init__(self)
        self.image = image
        self.mask = mask
        
    def loop(self, screen):
        from engine.level_manager import get_level
        image_pos = self.image.pos-get_level().screen_pos
        
        mask_pos = self.mask.pos-get_level().screen_pos
        img_manager.show_mask_img(screen=screen, 
                                  bg=self.image, 
                                  mask=self.mask, 
                                  bg_rect=Rect(image_pos, self.image.size), 
                                  mask_rect=Rect(mask_pos, self.mask.size),
                                  blend_mode=BLEND_MODE.MULTIPLY)
    @staticmethod
    def parse_image(image_data, pos, size, angle):
        if pos is None:
            log("Invalid arg pos not defined for Image",1)
            return None
        image_descriptor = get_element(image_data, "bg")
        pos = get_element(image_descriptor, "pos")
        size = get_element(image_descriptor, "size")
        angle = get_element(image_descriptor, "angle")
        if angle is None:
            angle = 0
        image = Image.parse_image(image_descriptor, pos, size, angle)
        image.init_image(size)
        
        mask_descriptor = get_element(image_data, "mask")
        pos = get_element(mask_descriptor, "pos")
        size = get_element(mask_descriptor, "size")
        angle = get_element(mask_descriptor, "angle")
        if angle is None:
            angle = 0
        mask = Image.parse_image(mask_descriptor, pos, size, angle)
        mask.init_image(size)
        
        return MaskImage(image, mask)

        
        