"""
Text class inherit from GameObject
"""
from engine import level_manager
from engine.font_manager import font_manager
from engine.img_manager import img_manager
from game_object.game_object_main import GameObject
from engine.vector import Vector2


class Text(GameObject):
    def __init__(self,pos,size,font,text,angle=0,color=(0,0,0),gradient=0,center=False,relative=False):
        GameObject.__init__(self)
        self.pos = pos
        self.center = center
        self.character_size = size
        self.color = color
        self.font = font_manager.load_font(font,self.character_size)
        self.text_surface = None
        self.set_text(text)
        self.gradient = gradient
        self.time = 1
        self.screen_relative = relative
        

    def set_text(self,text):
        self.text = text
        self.time = 1
        self.change_text(text)

    def change_color(self,color):
        self.change_text(self.text,color)

    def change_text(self,text,color=None):
        
        new_color = color
        if color == None:
            new_color = self.color
        if new_color == None:
            new_color = (0,0,0)

        if text != '':
            self.text_surface = font_manager.load_text(self.font,text,new_color,self.character_size,old_txt=self.text_surface)
        else:
            self.text_surface = None
            return
        if self.text_surface:
            self.size = font_manager.get_size(self.text_surface)
            if self.size:
                self.update_rect()

    def loop(self,screen):
        if self.text_surface:
            if self.time < self.gradient:
                #TODO work with time not frame
                self.time += 1
                self.change_text(self.text[0:int(self.time/self.gradient*len(self.text))])
            pos = self.pos
            if not self.screen_relative:
                pos = pos-level_manager.get_level().screen_pos
            if self.center and self.size:
                pos = pos - Vector2(self.size.x/2.0, 0.0)

            font_manager.show_text(self.text_surface,
                                   screen,
                                   pos,
                                   angle=self.angle,
                                   center=self.center,
                                   size=self.size)

    @staticmethod
    def parse_image(image_data, pos, size, angle):
        return None
