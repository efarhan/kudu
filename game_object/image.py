"""
Image class inherit from GameObject and use ImageManager to draw
"""

from animation.animation_main import Animation
from engine.img_manager import img_manager

from engine.init import engine
from engine.level_manager import get_level
from engine.rect import Rect
from game_object.game_object_main import GameObject
from engine.const import log, CONST
from json_export.json_main import get_element
from engine.vector import Vector2


class Image(GameObject):
    def __init__(self,
                 pos,
                 size=None,
                 angle=0,
                 relative=False,
                 path=""):
        GameObject.__init__(self)
        self.anim = None
        self.flip = False
        self.img = None
        self.tmp = False
        self.angle = angle
        if not isinstance(pos, Vector2):
            if isinstance(pos[0], list) or isinstance(pos[0], tuple):
                self.pos = Vector2(pos[0])
                self.screen_relative_pos = Vector2(pos[1])
            else:
                self.pos = Vector2(pos)
        else:
            self.pos = pos
        self.path = path
        self.size = Vector2(size)
        self.screen_relative = relative
        
        self.center_image = False
        self.update_rect()

    def init_image(self,size=None):
        if self.anim:
            self.anim.load_images(size,self.tmp)
        else:
            self.img = img_manager.load_image(self.path, self.tmp)
            if self.size is None:
                self.size = Vector2(img_manager.get_size(self.img))
            self.rect = Rect(self.pos, self.size)

    def loop(self, screen):
        if self.anim:
            self.anim.update_animation()
            self.img = self.anim.img


        pos = Vector2()
        if self.pos:
            pos = self.pos
        
        if self.screen_relative_pos is not None:
            pos = pos + self.screen_relative_pos * engine.screen_size

        if self.screen_relative:
            pos = self.pos
        else:
            pos = pos - get_level().screen_pos
        
        center_image = False
        try:
            center_image = self.center_image
        except AttributeError:
            pass

        img_manager.show_image(image=self.img,
                   screen=screen,
                   pos=pos,
                   new_size=self.size,
                   center_image=center_image,
                   angle=self.angle,
                   flip=self.flip)

        GameObject.loop(self, screen)

    @staticmethod
    def parse_image(image_data, pos, size, angle):
        if pos is None:
            log("Invalid arg pos not defined for Image",1)
            return None
        path = get_element(image_data, "path")
        image = Image(pos, size=size, angle=angle)

        if path is not None:
            path = CONST.path_prefix+path
            image.path = path



        tmp = get_element(image_data, "tmp")
        if tmp is not None:
            image.tmp = tmp
        else:
            image.tmp = False

        anim_data = get_element(image_data, "anim")
        if anim_data:
            image.anim = Animation.parse_animation(anim_data, image)
        image.init_image(size)
        return image
        


