try:
    import gc
    from engine.const import CONST, log
    from engine.img_manager import img_manager
    from game_object.game_object_main import GameObject
except ImportError as e:
    from engine.const import log
    log(e, 1)
__author__ = 'Elias'


class Video(GameObject):
    def __init__(self, path="",loop=False,framerate=CONST.framerate):
        GameObject.__init__(self)
        self.path = path
        self.vid_loop = loop
        self.capture = None
        self.framerate = framerate
        self.video = None

    def init(self):
        log("Init Video: "+self.path)
        self.video = img_manager.init_video(self.path)
        if self.video is None:
            log("Error while loading "+self.path, 1)

    def loop(self,screen):
        return img_manager.show_video(self.video, screen)
    
    def exit(self):
        
        img_manager.exit_video(self.video)
        del self.video
