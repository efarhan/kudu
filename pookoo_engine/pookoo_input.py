'''
Created on Jan 8, 2015

@author: efarhan
'''

from engine.const import log, enum
from engine.vector import Vector2
from engine.input_manager import InputManager
from pookoo import input


__author__ = 'efarhan'


keycodes = {'0': 26,
 'LALT': 39,
 'LCTRL': 37,
 'LSHIFT': 38,
 'LSYS': 40,
 'RALT': 43,
 'RCTRL': 41,
 'RSHIFT': 42,
 'RSYS': 44,
 'a': 0,
 'backspace': 59,
 'down': 74,
 'enter': 58,
 'escape': 36,
 'f1': 85,
 'left': 71,
 'pad0': 75,
 'right': 72,
 'tab': 60,
 'up': 73}


AXIS_TYPE = enum('TRIGGER', 'STICK', 'HAT')
class PookooInput(InputManager):
    """SFML Input Class"""
    def __init__(self):
        InputManager.__init__(self)
        self.joysticks = []

    
        

    def init_joystick(self):
        InputManager.init_joystick(self)
        input.joystick.begin()
        self.axis_definition = {}
        for joy in range(input.joystick.count()):
            self.joysticks.append(input.joystick.Joystick(joy))
        for j,joy in enumerate(self.joysticks):

            for i in range(joy.axis_count()):
                if joy.axis_get(i)< -75:
                    self.axis_definition['JOY'+str(j)+'AXIS'+str(i)] = AXIS_TYPE.TRIGGER
                else:
                    self.axis_definition['JOY'+str(j)+'AXIS'+str(i)] = AXIS_TYPE.STICK
        self.update_joy_event()
    def init_keyboard(self):
        InputManager.init_keyboard(self)

        for key in keycodes.keys():
            self.button_value[key] = 0
            self.button_key[keycodes[key]] = key
        for key in range(ord('z')-ord('a')+1):
            self.button_value[chr(ord('a')+key)] = 0
            self.button_key[keycodes['a']+key] = chr(ord('a')+key)
        for key in range(ord('9')-ord('0')+1):
            self.button_value[chr(ord('0')+key)] = 0
            self.button_key[keycodes['0']+key] = chr(ord('0')+key)
        for key in range(ord('9')-ord('0')+1):
            self.button_value["pad"+chr(ord('0')+key)] = 0
            self.button_key[keycodes['pad0']+key] = "pad"+chr(ord('0')+key)
        for key in range(1,12+1):
            self.button_value["f"+str(key)] = 0
            self.button_key[keycodes['f1']+key] = "f"+str(key)





    def update_keyboard_event(self,event=None):
        global keycodes

        for key in self.button_key.keys():
            self.button_value[self.button_key[key]] = input.keyboard.pressed(key)




    def get_current_axis(self):
        current_joys = []
        for axis in self.axis.keys():
            try:

                if self.axis_definition[axis] == AXIS_TYPE.TRIGGER:
                    trigger_command = "JOY_%s_AXIS_%s_"%(axis[3],axis[8])
                    if self.axis[axis] > 50:

                        current_joys.append(trigger_command+"+")
                elif self.axis_definition[axis] == AXIS_TYPE.STICK:
                    trigger_command = "JOY_%s_AXIS_%s_"%(axis[3],axis[8])
                    if self.axis[axis] > 50:
                        current_joys.append(trigger_command+"+")
                    elif self.axis[axis] < -50:
                        current_joys.append(trigger_command+"-")
            except KeyError:
                log("Error while getting: "+axis,1)
        return current_joys



    def add_one_key(self, key_value):
        """
        Add only one key
        """
        self.button_value[key_value] = 0
        try:
            if ord('a') <= ord(key_value) <= ord('z'):
                self.button_key[ord(key_value) - ord('a') + keycodes['a']] = key_value
            elif ord('0') <= ord(key_value) <= ord('9'):
                self.button_key[ord(key_value) - ord('0') + keycodes['0']] = key_value
        except TypeError:
            '''the key value is not a letter or a number'''
            try:
                self.button_key[keycodes[key_value]] = key_value
            except IndexError:
                pass
            except KeyError:
                pass

    def add_key_button(self,action,key_list):
        self.button_map[action] = key_list
        for key_value in key_list:
            keys = key_value.split("+")
            for k in keys:
                self.add_one_key(k)
    """MOUSE"""

    def get_mouse(self):
        """
        Return mouse state as
        position, (left, right,middle)
        """
        from engine.init import engine

        mouse_pos = Vector2(input.mouse.position())
        mouse_pos = (mouse_pos - engine.get_origin_pos())*(engine.screen_size.y/(engine.real_screen_size.y-2*engine.get_origin_pos().y))
        mouse_buttons = [input.mouse.pressed(0),
                        input.mouse.pressed(1),
                        input.mouse.pressed(2)]
        return mouse_pos, mouse_buttons

    def show_mouse(self,show=True):
        """
        Show/hide mouse
        """
        pass

    """JOYSTICK"""

    def update_joy_event(self):
        for j,joy in enumerate(self.joysticks):
            for i in range(joy.axis_count()):

                if i < 6:
                    self.axis['JOY'+str(j)+'AXIS'+str(i)] = joy.axis_get( i)
                else:
                    self.axis['JOY'+str(j)+'HAT'+str(i-6)] =joy.axis_get( i)
            for i in range(joy.button_count()):
                self.button_value['JOY'+str(j)+'BUTTON'+str(i)] = joy.button_get(i)

    def get_joy_button(self,action):

        try:
            button_key_list = self.button_map[action]
            value = False
            for button_key in button_key_list:
                if 'BUTTON' in button_key:

                    value = value or self.button_value["".join(button_key.split('_'))]
                elif 'AXIS' in button_key:
                    parameters = button_key.split('_')
                    axis_name = "".join(parameters[0:len(parameters)-1])

                    if parameters[-1] == '-':
                        value = value or self.axis[axis_name] < -50
                    elif parameters[-1] == '+':
                        value = value or self.axis[axis_name] > 50
                elif 'HAT' in button_key:
                    pass
            return value
        except KeyError:
            pass

        return False

    def add_joy_button(self,action, button_list):
        self.button_map[action] = button_list