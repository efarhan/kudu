from engine.snd_manager import SndManager
from pookoo import audio
from engine.const import log
__author__ = 'Elias Farhan'


class PookooSndManager(SndManager):
    def __init__(self):
        SndManager.__init__(self)
        audio.begin()
        self.fade_out_audio = []
        self.fade_in_audio = []
    def update_music_status(self):
        audio.step()
        if self.music is not None:
            if self.music.status() == "stopped":
                status = True
                if len(self.playlist != 0):
                    self.music_index = (self.music_index + 1) % len(self.playlist)
                    try:
                        self.music = audio.Stream(self.playlist[self.music_index])
                    except IndexError:
                        pass
        delete_sounds = []
        for s in self.sounds_playing:
            if s.status() == "stopped":
                delete_sounds.append(s)
        for s in delete_sounds:
            self.sounds_playing.remove(s)
            try:
                self.fade_out_audio.remove(s)
            except ValueError:
                pass
            try:
                self.fade_in_audio.remove(s)
            except ValueError:
                pass
        del delete_sounds[:]

        tmp_out = []
        for music in self.fade_out_audio:
            if music.volume_get() >= 0.0:
                music.volume_set( (music.volume_get()*100 - 0.75)/100.0)
                if music.volume_get() <= 0.0:
                    music.volume_set(0.0)
                    tmp_out.append(music)
        for s in tmp_out:
            try:
                self.fade_out_audio.remove(s)
            except ValueError:
                pass

        tmp_in = []
        for music in self.fade_in_audio:
            if music.volume_get() >= 0.0:
                music.volume_set((music.volume_get()*100 + 3)/100.0)
                if music.volume_get() >= 1.0:
                    music.volume_set(1.0)
                    tmp_in.append(music)
        for s in tmp_in:
            try:
                self.fade_in_audio.remove(s)
            except ValueError:
                pass
        

    def add_music_to_playlist(self,new_playlist):
        pass

    def set_playlist(self,music_list):
        self.playlist = music_list
        self.fade_out(self.music)
        try:
            self.music = audio.Stream(self.playlist[0])
        except IndexError:
            pass
        self.fade_in(self.music)


        
    def load_sound(self, name, permanent=False):
        """Load a sound in the system and returns it"""
        try:
            self.sounds[name]
        except KeyError:
            try:
                self.sounds[name] = audio.Sound(name)
            except IOError:
                log("Error: Importing "+name,1)
                return None
            except AttributeError:
                return None
        if permanent:
            self.permanent_sound.append(name)
        return self.sounds[name]

    def get_music_status(self):
        return self.music.status == "stopped"

    def fade_out(self, music):
        if music is not None:
            music.volume_set(1.00)
            self.fade_out_audio.append(music)

    def fade_in(self, music):
        if music is not None:
            music.volume_set(0.01)
            self.fade_in_audio.append(music)

    def play_sound(self, sound):
        """
        Plays a given sound
        """
        if sound is not None:
            sound.play()

    def stop_sound(self,sound):
        if sound is not None:
            sound.stop()
