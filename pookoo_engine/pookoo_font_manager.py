from engine.font_manager import FontManager
from pookoo import font
from pookoo import draw
from engine.init import engine
from engine.vector import Vector2
from engine.const import log

class PookooText():
    def __init__(self, font, text, color=(0, 0, 0), pixel_height=20):
        self.font = font
        self.text = text
        self.color = color
        self.pixel_height = pixel_height
        self.cache_size = 0
    

class PookooFontManager(FontManager):
    def __init__(self):
        FontManager.__init__(self)
    
    def load_font(self, name, pixel_size):
        font_instance = font.Font(name)
        font_instance.facesize(int(pixel_size))
        
        return font_instance
    
    def load_text(self, font, text, color=(0, 0, 0), pixel_height=20,old_txt=None):
        new_text = PookooText(font,text,color,pixel_height)
        if old_txt is not None:
            new_text.cache_size = old_txt.cache_size
        return new_text
        
    def get_size(self, text):
        return Vector2(text.font.size(text.text))
    
    def show_text(self, pookoo_text, screen, pos, angle=0, center=False, center_image=False,flip=False, size=None):
        
        origin_pos = engine.get_origin_pos()
        screen_diff_ratio = engine.get_ratio()
        text_size = pookoo_text.pixel_height
        
        draw.push()
        
        new_pos = (origin_pos + (pos) * screen_diff_ratio)
        size = text_size * screen_diff_ratio
        if pookoo_text.cache_size == 0 or pookoo_text.cache_size != int(size):
            
            pookoo_text.font.facesize(int(size))
            pookoo_text.cache_size = int(size)
        draw.move((new_pos+Vector2(0,int(size))).get_list())
    
        color = pookoo_text.color
        try:
            draw.color(color[0]/255.0,
                       color[1]/255.0,
                       color[2]/255.0,
                       color[3]/255.0)
        except IndexError:
            draw.color(color[0]/255.0,
                       color[1]/255.0,
                       color[2]/255.0,
                       1.0)
        pookoo_text.font.render(pookoo_text.text)
        draw.pop()
