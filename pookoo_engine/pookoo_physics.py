import math
import gc
from engine.const import CONST, log
from engine.physics_manager import PhysicsManager, pixel2meter, BodyType, meter2pixel,\
    MoveType
from engine.vector import Vector2
from event.physics_event import add_physics_event


from pookoo import physics
__author__ = 'Elias Farhan'

remove = 0
class PookooPhysicsManager(PhysicsManager):
    def __init__(self):
        PhysicsManager.__init__(self)
        self.vel_iters = 10
        self.pos_iters = 10
        try:
            self.time_step = 1.0/CONST.framerate
        except ZeroDivisionError:
            self.time_step = 1.0/60.0


    def init_world(self, gravity_arg=None):
        gravity_value = Vector2(0,0)
        if(gravity_arg is None):
            gravity_value = Vector2(0,CONST.gravity)
        else:
            gravity_value = gravity_arg
        world = physics.World(gravity_value.get_tuple(),self.vel_iters,self.pos_iters)
        self.worlds.append(world)
        self.current_world = world

    def loop(self, dt=CONST.framerate):
        PhysicsManager.loop(self,dt)
        for event in physics.get_events():
            add_physics_event(event)
        self.current_world.step(1.0/dt)

    def remove_world(self,world):
        try:
            self.worlds.remove(world)
        except ValueError:
            pass
        if world == self.current_world:
            del world
            self.current_world = None

    def remove_body(self,body):
        global remove
        log("REMOVE "+str(remove)+" "+str(body))
        remove += 1
        if body is not None:
            self.current_world.remove_body(body)

    def add_body(self,pos,body_type,angle=0,fixed_rotation=True):
        physic_position = pixel2meter(pos).get_float_tuple()
        radian_angle = angle*math.pi/180.0
        if body_type == BodyType.static:
            body = self.current_world.add_body(physic_position,radian_angle,"static")
        elif body_type == BodyType.dynamic:
            body = self.current_world.add_body(physic_position,radian_angle,"dynamic")
        return body
    
    def set_angle(self, body, angle):
        body.set_angle(angle*math.pi/180.0)

    @staticmethod
    def add_box(body,pos,size,angle=0,user_data=0,sensor=False):
        if not (body and pos and size):
            log("Invalid arg body pos size in box creation",1)
            return None
        center_pos = pixel2meter(pos)
        semi_size = pixel2meter(size)
        radian_angle = angle*math.pi/180.0
        return body.add_box(center_pos.get_tuple(), 
                            semi_size.get_tuple(),
                            radian_angle,
                            sensor,
                            user_data)

    @staticmethod
    def add_circle(body,pos,radius,user_data=0,sensor=False):
        if not (body and pos and radius):
            log("Invalid arg body pos size in box creation",1)
            return None
        center_pos = pixel2meter(pos)
        physics_radius = pixel2meter(radius)
        return body.add_circle(pos.get_tuple(), 
                               physics_radius,
                               sensor,
                               user_data)

    def get_body_position(self, body):
        return meter2pixel(Vector2(body.get_position()))
    
    def set_body_position(self, body, new_pos):
        body.set_position(pixel2meter(new_pos).get_list())
    
    def exit(self):
        log("EXIT PHYSICS")
        for world in self.worlds:
            del world
        del self.worlds
        
    def move(self, body, vx=None, vy=None, type=MoveType.force):
        from engine.init import engine
        if vx is not None:
            if vx == 0:
                body.stop(0,1,1.0/engine.get_framerate(),type)
            else:
                body.move(vx,0.0,1.0/engine.get_framerate(),type)
        if vy is not None:
            if vy == 0:
                
                body.stop(1,0,1.0/engine.get_framerate(),type)
            else:
                body.move(0,vy,1.0/engine.get_framerate(),type)
    def jump(self, body, vy):
        body.jump(vy)
        
        
        
        
