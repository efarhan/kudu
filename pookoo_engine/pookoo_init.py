from pookoo import window
from pookoo import draw
from pookoo import sync
from pookoo import texture
from pookoo import audio
from pookoo import input

import time
import math

from engine.init import Engine
from engine.vector import Vector2
from engine.const import CONST, log
from engine import level_manager
from engine.input_manager import input_manager



class PookooEngine(Engine):
    def init_screen(self):
        # Open the window
        if not window.begin():
            log('Failed to open window :(', 1)
            exit()
            
        if not sync.begin():
            log('Failed to initialize sync :(', 1)
            exit()
            
        self.real_screen_size = Vector2(window.size())
        if CONST.framerate != 0:
            self.clock = sync.Sync(CONST.framerate)
            self.delta_time = 1.0/CONST.framerate
        self.delta_time = 1.0/60.0
        self.last_frame = time.time()
        Engine.init_screen(self)
        self.begin = time.time()
        input_manager.init_joystick()
        if not CONST.debug and CONST.fullscreen and not window.is_fullscreen():
            window.toggle_fullscreen()

        if not CONST.fullscreen and window.is_fullscreen():
            window.toggle_fullscreen()

    def init_level(self):
        from scenes.gamestate import GameState

        if CONST.debug:
            level_manager.switch_level(GameState(CONST.startup))
        else:
            Engine.init_level()
            
    def pre_update(self):
        draw.clear()
        draw.blend.normal()
        self.real_screen_size = Vector2(window.size())
        input_manager.update_keyboard_event()
        input_manager.update_joy_event()
        Engine.init_screen(self)
        """log(str(self.real_screen_size)+" "+
            str(self.screen_size)+" "+
            str(self.get_origin_pos())+" "+
            str(self.get_ratio()))"""
            
    def post_update(self):
        
        first_pos = [0.0,0.0]
        first_size = [0.0,0.0]
        second_pos = [0.0,0.0]
        second_size = [0.0,0.0]
        
        
        if self.get_origin_pos().y > self.get_origin_pos().x:
            first_size = [self.real_screen_size.x, self.get_origin_pos().y]
            second_pos= [0, self.real_screen_size.y - self.get_origin_pos().y]
            second_size = first_size
        else:
            first_size = [self.get_origin_pos().x, self.real_screen_size.y]
            second_pos = [self.real_screen_size.x - self.get_origin_pos().x, 0]
            second_size = first_size

        

        draw.push()
        draw.move(first_pos)
        draw.color(0.0,0.0,0.0)
        draw.rectangle(first_size)
        draw.pop()
        
        draw.push()
        draw.move(second_pos)
        draw.color(0.0,0.0,0.0)
        draw.rectangle(second_size)
        draw.pop()
        
        
        self.status = window.step()
        
        if CONST.framerate != 0:
            self.clock.step()
        
        current_frame = time.time()
        self.delta_time = current_frame-self.last_frame
        self.last_frame = current_frame
        self.finish = not self.status
        
    def get_framerate(self):
        #log(self.delta_time)
        return 1.0/self.delta_time
    
    def set_framerate(self, framerate):
        self.clock.set(int(1.0/framerate*1000000000))
    
    def exit(self):
        Engine.exit(self)
        if CONST.framerate:
            del self.clock
        from engine.img_manager import img_manager
        img_manager.exit()
        input.joystick.finish()
        audio.finish()
        texture.finish()
        draw.finish()
        sync.finish()
        window.finish()
