'''
Created on 9 janv. 2015

@author: efarhan
'''
import copy
from pookoo import draw
from pookoo import texture
from pookoo import video
import gc

from engine.img_manager import ImgManager
from engine.const import log
from engine.vector import Vector2
from engine.init import engine

class PookooImgManager(ImgManager):
    def __init__(self):
        ImgManager.__init__(self)
        if not draw.begin():
            log("Failed to open draw :(", 1)
        texture.begin()



    def init_video(self, name):
        if not video.begin():
            log('Failed to init video :(', 1)
        self.old_t = 0
        return video.Video(name)

    def show_video(self, video_instance, screen, pos=Vector2(), new_size=engine.screen_size):
        if video_instance is None:
            return True
        draw.blend.normal()
        draw.push()

        origin_pos = engine.get_origin_pos()
        screen_diff_ratio = engine.get_ratio()

        pos = (origin_pos + (pos) * screen_diff_ratio)
        size = new_size * screen_diff_ratio

        draw.move((pos).get_list())

        texture_size = Vector2(video_instance.size())
        image_scale = (size/texture_size).get_list()
        draw.scale(image_scale[0], image_scale[1])
        new_t = video_instance.next()

        draw.pop()

        if new_t is None:
            return True
        else:
            engine.clock.set(new_t - self.old_t)
            self.old_t = new_t
            return False
    
    def exit_video(self, video_instance):

        video.finish()
    
    def load_image(self, name, tmp=False):
        #log("Loading image: "+name)
        try:
            self.img_name[name]
        except KeyError:
            try:
                #log("Load sfml texture: "+name)
                self.img_name[name] = texture.Texture(name)
            except IOError as e:
                log(str(e), 1)
                return None
            except AttributeError as e:
                log(str(e), 1)
                return None
            if tmp:
                self.tmp_images.append(name)

        return self.img_name[name]
        
    def show_image(self, image, screen, pos, angle=0, center=False, new_size=None, center_image=False, flip=False):
        if image is None or new_size is None:
            return

        """Check position topleft and bottomright"""
        topleft_pos = (Vector2()+pos)
        bottomright_pos = (pos+new_size)

        if topleft_pos.x >= bottomright_pos.x or topleft_pos.y >= bottomright_pos.y:
            return

        if topleft_pos.x < 0 and bottomright_pos.x < 0:
            return
        if topleft_pos.x > engine.screen_size.x and bottomright_pos.x > engine.screen_size.x:
            return
        if topleft_pos.y < 0 and bottomright_pos.y < 0:
            return
        if topleft_pos.y > engine.screen_size.y and bottomright_pos.y > engine.screen_size.y:
            return

        draw.blend.normal()
        draw.push()
        origin_pos = engine.get_origin_pos()
        screen_diff_ratio = engine.get_ratio()
        pos = (origin_pos + (pos) * screen_diff_ratio)
        size = new_size * screen_diff_ratio
        draw.move((pos+size/2).get_list())
        draw.rotate(angle)
        draw.move((Vector2()-size/2).get_list())
        texture_size = Vector2(image.size())
        image_scale = (size/texture_size).get_list()
        draw.scale(image_scale[0], image_scale[1])
        image.render(flip)
        
        draw.pop()
    
    def draw_rect(self, screen, rect, color, angle=0):
        draw.blend.normal()
        draw.push()
        origin_pos = engine.get_origin_pos()
        screen_diff_ratio = engine.get_ratio()
        pos = (origin_pos + (rect.pos) * screen_diff_ratio)
        size = rect.size * screen_diff_ratio
        draw.move((pos+size/2).get_list())
        draw.rotate(angle)
        draw.move((Vector2()-size/2).get_list())
        try:
            draw.color(color[0]/255.0,
                       color[1]/255.0,
                       color[2]/255.0,
                       color[3]/255.0)
        except IndexError:
            draw.color(color[0]/255.0,
                       color[1]/255.0,
                       color[2]/255.0,
                       1.0)
        draw.rectangle(size.get_list())
        
        draw.pop()
    def get_size(self, image):
        return Vector2(image.size())
    
    def show_mask_img(self, 
        screen, 
        bg, 
        mask, 
        bg_rect,
        mask_rect, 
        bg_angle=0, 
        mask_angle=0, 
        blend_mode=0, 
        mask_type=0):
        origin_pos = engine.get_origin_pos()
        screen_diff_ratio = engine.get_ratio()
        
        draw.blend.masker()
        draw.push()
        pos = (origin_pos + (mask_rect.pos) * screen_diff_ratio)
        size = mask_rect.size * screen_diff_ratio
        draw.move((pos+size/2).get_list())
        draw.rotate(mask_angle)
        draw.move((Vector2()-size/2).get_list())
        texture_size = Vector2(mask.img.size())
        image_scale = (size/texture_size).get_list()
        draw.scale(image_scale[0], image_scale[1])
        mask.img.render(0)
        draw.pop()
        
        draw.blend.masked()
        draw.push()
        pos = (origin_pos + (bg_rect.pos) * screen_diff_ratio)
        size = bg_rect.size * screen_diff_ratio
        draw.move((pos+size/2).get_list())
        draw.rotate(bg_angle)
        draw.move((Vector2()-size/2).get_list())
        texture_size = Vector2(bg.img.size())
        image_scale = (size/texture_size).get_list()
        draw.scale(image_scale[0], image_scale[1])
        bg.img.render(0)
        draw.pop()
        
    def exit(self):
        self.img_name.clear()
        del self.img_name