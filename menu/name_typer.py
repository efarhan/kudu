import json
import os

from engine import stat, level_manager, img_manager, input_manager
from engine.const import log, CONST
from engine.img_manager import img_manager
from engine.init import engine
from engine.input_manager import input_manager
from engine.rect import Rect
from engine.vector import Vector2
from game_object.game_object_main import GameObject
from game_object.text import Text
from engine.scene import Scene
from json_export import json_main
from menu.menustate import MenuState


__author__ = 'efarhan'


class NameTyper(Scene):
    def __init__(self):
        self.letters = [chr(i) for i in range(ord('a'),ord('z')+1)]
        self.letters.extend([chr(i) for i in range (ord('A'),ord('Z')+1)])
        self.letters.extend([chr(i) for i in range(ord('0'),ord('9')+1)])
        self.letters.extend(['-', ',', '.', '(', ')', '[', ']', '{', '}', '/'])
        self.characters = []
        self.character_size = 100
        self.screen_pos = Vector2()

        self.letters_origin = Vector2(100,100)
        self.cursor = Cursor(self)

        self.pseudoname = ""
        self.finish = False
        self.pseudotext = None
    def init(self, loading=False):
        self.x_nmb = (engine.get_screen_size().x-self.letters_origin.x)/self.character_size
        self.size_x = (engine.get_screen_size().x-self.letters_origin.x)-(engine.get_screen_size().x-self.letters_origin.x)%(self.character_size*self.x_nmb)
        for i, l in enumerate(self.letters):

            pos = self.letters_origin + Vector2((self.character_size*i)%self.size_x, self.character_size*(i/self.x_nmb+1))
            self.characters.append(Text(pos=pos,size=self.character_size,font="data/font/pixel_arial.ttf",text=l,color=(255,255,255)))
        #read pseudoname if it exists
        if os.path.exists('data/saves/saves.json'):
            try:
                name_file = json_main.load_json('data/saves/saves.json')
                self.pseudoname = name_file["pseudo"]
            except Exception:
                log("No pseudoname could be read",1)
        self.pseudotext = Text(pos=Vector2(), size=self.character_size,font="data/font/pixel_arial.ttf",text="name:"+self.pseudoname,color=(255,255,255))

    def loop(self, screen):
        for c in self.characters:
            c.change_color((255,255,255))
        self.pseudotext.set_text(text="name:"+self.pseudoname)
        self.cursor.loop(screen)
        if self.cursor.time < self.cursor.frequency/2:
            self.characters[self.cursor.array_index].change_color((0,0,0))
        for c in self.characters:
            c.loop(screen)
        self.pseudotext.loop(screen)
        if self.finish:
            #go to next scenes
            stat.set_value("pseudo", self.pseudoname)
            saves = json_main.load_json('data/saves/saves.json')
            if saves is None:
                if not os.path.exists("data/saves"):
                    os.makedirs("data/saves")

            saves["pseudo"] = self.pseudoname
            id_file = open('data/saves/saves.json', 'w')
            id_file.write(json.dumps(saves))
            id_file.close()
            level_manager.switch_level(MenuState())


class Cursor(GameObject):
    def __init__(self, name_typer):
        GameObject.__init__(self)
        self.name_typer = name_typer
        self.time = 0
        self.frequency = CONST.framerate/2
        self.size = Vector2(1,1)*self.name_typer.character_size
        self.array_index = 0
        self.pos = self.name_typer.letters_origin+Vector2(-10,self.size.y)
        self.directions = [False for i in range(7)]

    def loop(self,screen):
        LEFT = input_manager.get_button('left')
        RIGHT = input_manager.get_button('right')
        UP = input_manager.get_button('up')
        DOWN = input_manager.get_button('down')
        FIRST = input_manager.get_button('first_action')
        SECOND = input_manager.get_button('second_action')
        START = input_manager.get_button('start_menu')

        #manage the movement of the cursor

        if not self.directions[0] and LEFT:
            if self.array_index > 0:
                self.array_index -= 1
                if (self.array_index+1)%int(self.name_typer.x_nmb) == 0:
                    self.pos = Vector2(self.name_typer.x_nmb*self.size.x,self.pos.y-self.size.y)
                else:
                    self.pos.x -= self.name_typer.character_size

            else:
                self.array_index = self.name_typer.x_nmb-1
                self.pos = Vector2(self.name_typer.x_nmb*self.size.x,self.pos.y)
        elif not self.directions[1] and RIGHT:
            if self.array_index < len(self.name_typer.characters)-1:
                self.array_index += 1
                if self.array_index%int(self.name_typer.x_nmb) == 0:
                    self.pos = Vector2(self.name_typer.letters_origin.x,self.pos.y+self.size.y)
                else:
                    self.pos.x += self.size.x
            else:
                self.array_index = len(self.name_typer.characters)-self.name_typer.x_nmb
                self.pos = Vector2(self.name_typer.letters_origin.x,self.pos.y)
        elif not self.directions[2] and UP:
            if self.array_index >= self.name_typer.x_nmb:
                self.pos.y -= self.name_typer.character_size
                self.array_index -= self.name_typer.x_nmb
            else:
                self.array_index = len(self.name_typer.characters)-self.name_typer.x_nmb+(self.array_index)%int(self.name_typer.x_nmb)
                self.pos.y += self.size.y*(len(self.name_typer.characters)/self.name_typer.x_nmb-1)
        elif not self.directions[3] and DOWN:
            if self.array_index <= len(self.name_typer.characters)-1-self.name_typer.x_nmb:
                self.pos.y += self.name_typer.character_size
                self.array_index += self.name_typer.x_nmb
            else:
                self.array_index = (self.array_index)%int(self.name_typer.x_nmb)
                self.pos.y -= self.size.y*(len(self.name_typer.characters)/self.name_typer.x_nmb-1)
        elif not self.directions[4] and FIRST and not stat.get_value("FIRST"):
            #add letter to pseudo
            self.name_typer.pseudoname += self.name_typer.letters[self.array_index]
        elif not self.directions[5] and SECOND:
            #remove one letter to pseudo
            self.name_typer.pseudoname = self.name_typer.pseudoname[0:len(self.name_typer.pseudoname)-1]
        elif not self.directions[6] and START:
            #finish name typing
            self.name_typer.finish = True
        stat.set_value("FIRST", FIRST)
        self.directions = [LEFT,RIGHT,UP,DOWN,FIRST,SECOND,START]


        if self.time < self.frequency/2:
            img_manager.draw_rect(screen=screen,rect=Rect(self.pos,self.size),color=(255,255,255))
        elif self.time == self.frequency:
            self.time = 0
        self.time += 1
