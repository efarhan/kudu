from engine import level_manager, stat, img_manager, input_manager
from engine.const import CONST
from engine.img_manager import img_manager
from engine.input_manager import input_manager
from engine.rect import Rect
from engine.vector import Vector2
from game_object.game_object_main import GameObject
from game_object.text import Text
from engine.scene import Scene
from json_export.json_main import load_json, write_json
from menu.menustate import MenuState

__author__ = 'Elias'


class InputMenu(Scene):
    """TODO: Refactor"""
    def __init__(self):
        Scene.__init__(self)

        self.actions = []
        self.keys = []
        self.letters_origin = Vector2(100,100)
        self.character_size = 100

        self.cursor = Cursor(self)
        self.actions = []
        self.values = []

        self.actions_file = {}

        self.input_timer = 60
        self.change_keys = {}

    def init(self, loading=False):
        size_x = 0
        self.actions_file = load_json(CONST.actions)
        for i, action in enumerate(self.actions_file.keys()):
            pos = self.letters_origin + Vector2(0,self.character_size)*i
            t = Text(pos=pos,
                     size=self.character_size,
                     font="data/font/pixel_arial.ttf",
                     text=action,
                     color=(255,255,255))
            self.actions.append(t)

            if t.size.x > size_x:
                size_x = t.size.x
        self.cursor.size.x = size_x
        for i,action in enumerate(self.actions_file.values()):
            pos = self.letters_origin + Vector2(0,self.character_size)*i + Vector2(size_x,0)
            t = Text(pos=pos,
                     size=self.character_size,
                     font="data/font/pixel_arial.ttf",
                     text=str(action),
                     color=(255,255,255))
            self.values.append(t)

    def loop(self, screen):
        for c in self.actions:
            c.change_color((255,255,255))
        self.cursor.loop(screen)
        if self.cursor.time < self.cursor.frequency/2 or self.cursor.select:
            self.actions[self.cursor.array_index].change_color((0,0,0))

        if self.cursor.select and not self.cursor.directions[2]:
            keys = input_manager.get_current_key()
            keys.extend(input_manager.get_current_axis())
            current_keys = []
            for k in keys:
                current_keys.append(k)
                try:
                    self.change_keys[k] += 1
                    if self.change_keys[k] > self.input_timer:
                        self.actions_file[self.actions[self.cursor.array_index].text_surface.string] = [k]
                        write_json(CONST.actions,self.actions_file)
                        self.values[self.cursor.array_index].change_text(str(self.actions_file[self.actions[self.cursor.array_index].text_surface.string]))
                        input_manager.update_actions()
                        self.cursor.select = False
                except KeyError:
                    self.change_keys[k] = 0
            for k in self.change_keys.keys():
                if k not in current_keys:
                    del self.change_keys[k]
        for c in self.actions:
            c.loop(screen)
        for v in self.values:
            v.loop(screen)


class Cursor(GameObject):
    def __init__(self,input_menu):
        GameObject.__init__(self)
        self.input_menu = input_menu
        self.array_index = 0
        self.size_x = 0
        self.pos = self.input_menu.letters_origin
        self.time = 0
        self.frequency = CONST.framerate/2
        self.size = Vector2(0,self.input_menu.character_size)
        self.select = False

        self.directions = [False for i in range(4)]
    def loop(self,screen):
        
        UP = input_manager.get_button('up')
        DOWN = input_manager.get_button('down')
        FIRST = input_manager.get_button('first_action')
        SECOND = input_manager.get_button('second_action')

        if not self.select:
            if UP and not self.directions[0]:
                if self.array_index == 0:
                    self.array_index = len(self.input_menu.actions)-1
                    self.pos.y += self.input_menu.character_size*(len(self.input_menu.actions)-1)
                else:
                    self.array_index -= 1
                    self.pos.y -= self.input_menu.character_size
            elif DOWN and not self.directions[1]:
                if self.array_index == len(self.input_menu.actions)-1:
                    self.array_index = 0
                    self.pos.y -= self.input_menu.character_size*(len(self.input_menu.actions)-1)
                else:
                    self.array_index += 1
                    self.pos.y += self.input_menu.character_size
            elif FIRST and not self.directions[2] and not stat.get_value("FIRST"):
                self.select = True
            elif SECOND and not self.directions[3]:
                level_manager.switch_level(MenuState())
        else:
            if SECOND and not self.directions[3]:
                self.select = False

        stat.set_value("FIRST",FIRST)

        self.directions = [UP,DOWN,FIRST,SECOND]



        if self.time < self.frequency/2 or self.select:
            img_manager.draw_rect(screen=screen,screen_pos=Vector2(),rect=Rect(self.pos,self.size),color=(255,255,255))
        elif self.time > self.frequency:
            self.time = 0
        self.time += 1