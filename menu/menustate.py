'''
Created on 11 janv. 2014

@author: efarhan
'''

import time
from engine import level_manager, stat
from engine.init import engine
from engine.const import enum, CONST, log
from engine.img_manager import img_manager
from engine.input_manager import input_manager
from engine.rect import Rect
from engine.scene import Scene
from engine.vector import Vector2
from game_object.game_object_main import GameObject
from game_object.image import Image
from game_object.text import Text
from engine.snd_manager import snd_manager
from scenes.credits import Credits

State = enum('MAIN', 'OPTIONS', 'GRAPHICS', 'SOUNDS', 'KEYS')


class MenuState(Scene):
    def __init__(self):
        Scene.__init__(self)
        self.state = None
        self.items = ["Play", "Credits", "Quit"]
        self.letters_origin = Vector2(100,500)

        self.character_size = 100
        self.menus = []
        self.cursor = Cursor(self)
        self.bg_color = (0,0,0)
        
        stat.set_value("FIRST", False)
        snd_manager.set_playlist(["data/sound/mast-intro-menu2.ogg", "data/sound/mast-loop-menu2.ogg"])
        self.bg = Image(pos=Vector2(), size=engine.screen_size, path="data/sprites/text/screen.png")
        self.bg.init_image()
        self.title = None

    def init(self):
        Scene.init(self)

        size_x = 0
        for i, item in enumerate(self.items):
            pos = self.letters_origin + Vector2(0,self.character_size)*i
            t = Text(pos=pos,
                     size=self.character_size,
                     font=CONST.font,
                     text=item,
                     color=(255,255,255)
                )
            self.menus.append(t)
            if t.size.x > size_x:
                size_x = t.size.x
        self.cursor.size.x = size_x
        self.title = Text(pos=Vector2(100,100), size=self.character_size*3/2,font=CONST.font, text="Power Creep", color=(255,255,255))

    def loop(self, screen):
        Scene.loop(self, screen)

        snd_manager.update_music_status()
        #log(engine.get_framerate())
        
        img_manager.draw_rect(screen, rect=Rect(Vector2(),size=engine.screen_size),color=self.bg_color)
        self.bg.loop(screen)
        self.title.loop(screen)
        for c in self.menus:
            c.change_color((255,255,255))
        self.cursor.loop(screen)
        if level_manager.get_level() == self:
            if not self.cursor.state:
                self.menus[self.cursor.array_index].change_color((0,0,0))
            for item in self.menus:
                item.loop(screen)


class Cursor(GameObject):
    def __init__(self, menu):
        GameObject.__init__(self)
        self.menu = menu
        self.array_index = 0
        self.size_x = 0
        self.pos = self.menu.letters_origin
        self.directions = [False for i in range(3)]

        self.last_frame = time.time()
        self.time = 0
        self.frequency = 0.5#fps
        self.size = Vector2(0,self.menu.character_size)
        self.state = False

    def loop(self,screen):
        UP = input_manager.get_button('up')
        DOWN = input_manager.get_button('down')
        FIRST = input_manager.get_button('first_action')

        if UP and not self.directions[0]:
            if self.array_index == 0:
                self.array_index = len(self.menu.menus)-1
                self.pos.y += self.menu.character_size*(len(self.menu.menus)-1)
            else:
                self.array_index -= 1
                self.pos.y -= self.menu.character_size
        elif DOWN and not self.directions[1]:
            if self.array_index == len(self.menu.menus)-1:
                self.array_index = 0
                self.pos.y -= self.menu.character_size*(len(self.menu.menus)-1)
            else:
                self.array_index += 1
                self.pos.y += self.menu.character_size
        elif FIRST and not self.directions[2]:
            stat.set_value("FIRST", True)
            from scenes.gamestate import GameState
            from menu.input_menu import InputMenu
            from menu.name_typer import NameTyper
            if self.array_index == 0:
                level_manager.switch_level(GameState(CONST.startup))
            elif self.array_index == 1:
                level_manager.switch_level(Credits())
            elif self.array_index == 2:
                level_manager.switch_level(None)

        self.directions = [UP,DOWN,FIRST]


        current_time = time.time()
        if current_time-self.last_frame < self.frequency/2:
            self.state = False
            
            img_manager.draw_rect(screen=screen,rect=Rect(self.pos,self.size),color=(255,255,255))
        elif self.frequency > current_time-self.last_frame >= self.frequency/2:
            self.state = True
        elif current_time-self.last_frame >= self.frequency:
            self.last_frame = current_time-(current_time-self.last_frame-self.frequency)

