from animation.animation_main import Animation
from engine import level_manager
from engine.init import engine
from engine.physics_manager import physics_manager
from event.physics_event import get_physics_event
from engine.input_manager import input_manager

__author__ = 'Elias'

class RPGAnimation(Animation):
    def __init__(self,player):
        Animation.__init__(self, player)
        self.foot = 0
        self.player = self.obj
        self.speed = 3
        self.h_dir, self.v_dir = True, True #True for right, True for down

    def load_images(self, size=None, tmp=False):
        Animation.load_images(self, size=size, tmp=tmp)

    def update_animation(self, state="", invert=False):
        self.update_state()
        return Animation.update_animation(self, state=state, invert=invert)

    def update_state(self):
        """Main prototyping part, managing the input, hte physics events, moving the object, moving the screen_pos"""
        RIGHT = input_manager.get_button('right')
        LEFT = input_manager.get_button('left')
        UP = input_manager.get_button('up')
        DOWN = input_manager.get_button('down')

        horizontal = RIGHT-LEFT
        vertical = UP-DOWN

        physics_events = get_physics_event()

        for event in physics_events:
            if (event.userDataA == 2 and event.userDataB == 11 ) or \
                    ( event.userDataB == 2 and event.userDataA == 11):
                if event.begin:
                    self.foot += 1
                else:
                    self.foot -= 1

        if horizontal == -1:
            self.h_dir = False
            self.state = 'left'
            physics_manager.move(self.player.body, -self.speed)
        elif horizontal == 1:
            self.h_dir = True
            self.state = 'right'
            physics_manager.move(self.player.body, self.speed)
        else:
            physics_manager.move(self.player.body, 0)
        if vertical == -1:
            self.v_dir = True
            self.state = 'down'
            self.player.flip = False
            physics_manager.move(self.player.body, vy=self.speed)
        elif vertical == 1:
            self.v_dir = False
            self.state = 'up'
            self.player.flip = False
            physics_manager.move(self.player.body, vy=-self.speed)
        else:
            physics_manager.move(self.player.body, vy=0)
        if vertical == 0 and horizontal == 0:
            self.state = 'down_still'


        physics_pos = physics_manager.get_body_position(self.player.body)

        if physics_pos:
            pos = physics_pos-self.player.size/2
        else:
            pos = self.player.pos
        if self.player.screen_relative_pos:
            pos = pos-self.player.screen_relative_pos*engine.get_screen_size()
        self.player.pos = pos

        self.set_screen_pos()

    def set_screen_pos(self):
        level_manager.level.screen_pos = self.player.pos

    @staticmethod
    def parse_special_animation(anim, anim_data):
        pass