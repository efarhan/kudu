"""
2D platformer template of a player
"""
import sys
import traceback
from engine import level_manager, stat
from engine.input_manager import input_manager
from engine.physics_manager import physics_manager
from engine.snd_manager import snd_manager

from event.physics_event import get_physics_event
from engine.init import engine
from animation.animation_main import Animation
from engine.const import log


class PlatformerAnimation(Animation):
    def __init__(self,player):
        Animation.__init__(self, player)
        self.foot = 0
        self.player = self.obj
        self.speed = 5
        self.jump = 10
        self.direction = True #True for right
        self.jump_clicked = False


    def load_images(self, size=None, tmp=False):
        Animation.load_images(self, size=size, tmp=tmp)
    
    def update_animation(self, state="", invert=False):
        self.update_state()
        return Animation.update_animation(self, state=state, invert=invert)

    def update_state(self):
        """Main prototyping part, managing the input, hte physics events, moving the object, moving the screen_pos"""
        RIGHT = input_manager.get_button('right')
        LEFT = input_manager.get_button('left')
        UP = input_manager.get_button('up')
        DOWN = input_manager.get_button('down')
        
        horizontal = RIGHT-LEFT
        vertical = UP-DOWN
        
        physics_events = get_physics_event()
        for event in physics_events:
            log(str(event)+" "+str(event.userDataA)+" "+str(event.userDataB)+" "+str(event.begin))
            
            if (event.userDataA == 2 and event.userDataB == 11 ) or \
                    ( event.userDataB == 2 and event.userDataA == 11):
                if event.begin:
                    self.foot += 1
                else:
                    self.foot -= 1
        
        if horizontal == -1:
            self.direction = False
            if self.foot:
                self.state = 'move'
            self.player.flip = True
            physics_manager.move(self.player.body, vx=-self.speed)
        elif horizontal == 1:
            self.direction = True
            if self.foot:
                self.state = 'move'
            self.player.flip = False
            physics_manager.move(self.player.body, vx=self.speed)
        
        else:
            if self.foot:
                if self.direction:
                    self.state = 'still'
                    self.player.flip = False
                else:
                    self.state = 'still'
                    self.player.flip = True
            physics_manager.move(self.player.body, vx=0)

        if vertical == 1:
            if self.foot and not self.jump_clicked:
                self.jump_clicked = True
                physics_manager.jump(self.player.body, vy=self.jump)
                
        if not self.foot:
            self.jump_clicked = False
            if self.direction:
                self.state = 'jump'
                self.player.flip = False
            else:
                self.state = 'jump'
                self.player.flip = True

        physics_pos = physics_manager.get_body_position(self.player.body)
        
        if physics_pos:
            pos = physics_pos-self.player.size/2
        else:
            pos = self.player.pos

        if self.player.screen_relative_pos:
            pos = pos-self.player.screen_relative_pos*engine.get_screen_size()
        self.player.pos = pos
        
        self.set_screen_pos()

    def set_screen_pos(self):
        physics_pos = physics_manager.get_body_position(self.player.body)

        if self.player.screen_relative_pos:
            pos = physics_pos-self.player.screen_relative_pos*engine.screen_size
        level_manager.level.screen_pos = pos
        if stat.get_value("mask") is not None:
            stat.get_value("mask").mask.pos = pos

    @staticmethod
    def parse_special_animation(anim, anim_data):
        pass