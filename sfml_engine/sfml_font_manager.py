from numbers import Number

import sfml

from engine.vector import Vector2
from engine.font_manager import FontManager
from engine.img_manager import img_manager


__author__ = 'Elias'


class SFMLFontManager(FontManager):
    def import_font(self,filename):
        return sfml.Font.from_file(filename)

    def load_text(self,font,text,color=(0,0,0),pixel_height=0,old_txt=None):
        if font and pixel_height:
            text = sfml.Text(text)
            text.font = font
            if isinstance(pixel_height,Number):
                text.character_size = self.pixel2point(pixel_height)
            elif isinstance(pixel_height,Vector2):
                text.character_size = self.pixel2point(pixel_height.y)
            text.color = sfml.Color(color[0], color[1], color[2])
            return text

        return None

    def get_size(self,text):
        sfml_size = (text.global_bounds.width,text.global_bounds.height)
        return Vector2(sfml_size)
    
    def show_text(self, text, screen, pos, angle=0, center=False, size=None, center_image=False, flip=False):
        img_manager.show_image(image=text,
                               screen=screen,
                               pos=pos,
                               angle=angle,
                               center=center,
                               new_size=size,
                               center_image=center_image,
                               flip=flip)
