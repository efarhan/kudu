from OpenGL.GL import *
import sfml

from engine import level_manager
from engine.const import CONST, log
from engine.init import Engine
from engine.input_manager import input_manager
from engine.level_manager import get_level
from engine.vector import Vector2


__author__ = 'Elias'


class SFMLEngine(Engine):
    def __init__(self):
        Engine.__init__(self)
        self.clock = sfml.Clock()
    def init_screen(self):

        desktop = sfml.VideoMode.get_desktop_mode()
        #if CONST.debug:
        desktop = sfml.VideoMode(500,500)
        style = sfml.Style.DEFAULT
        if CONST.fullscreen and not CONST.debug:
            style = sfml.Style.FULLSCREEN
        self.screen = sfml.RenderWindow(desktop, 'Kudu Window', style)
        log(self.screen.settings)
        self.real_screen_size = Vector2(self.screen.size)
        
        self.screen.vertical_synchronization = True
        input_manager.init()

        Engine.init_screen(self)
        self.screen.framerate_limit = CONST.framerate

    def init_level(self):
        from scenes.gamestate import GameState

        if CONST.debug:
            level_manager.switch_level(GameState(CONST.startup))
            #level_manager.switch_level(NameTyper())
        else:
            Engine.init_level()

    def pre_update(self):
        from engine.img_manager import img_manager
        self.delta_time = self.clock.restart().microseconds
        img_manager.clear_screen(self.screen)

    def post_update(self):
        #hide the sides
        first_rect = sfml.RectangleShape()
        first_rect.position = (0, 0)
        second_rect = sfml.RectangleShape()
        if self.origin_pos.y >= self.origin_pos.x:
            first_rect.size = (self.real_screen_size.x, self.origin_pos.y)
            second_rect.position = (0, self.real_screen_size.y - self.origin_pos.y)
            second_rect.size = first_rect.size
        else:
            first_rect.size = (self.origin_pos.x, self.real_screen_size.y)
            second_rect.position = (self.real_screen_size.x - self.origin_pos.x, 0)
            second_rect.size = first_rect.size
        first_rect.fill_color = sfml.Color.BLACK
        second_rect.fill_color = sfml.Color.BLACK
        self.screen.draw(first_rect)
        self.screen.draw(second_rect)



        self.screen.display()

    def exit(self):

        from engine.img_manager import img_manager

        img_manager.sanitize_img_manager(remove_all=True)
        self.screen.close()
        Engine.exit(self)

    def set_framerate(self, framerate):
        self.screen.framerate_limit = framerate

    def update_event(self):
        """
        Update the states of Input Event
        """
        window = self.screen
        input_manager.update_joy_event()
        for event in window.events:
            input_manager.update_keyboard_event(event)
            if type(event) is sfml.CloseEvent:
                from engine.init import engine

                self.finish = True
            elif type(event) is sfml.MouseButtonEvent:
                from engine.init import engine
                screen_ratio = float(engine.screen_size.y) / Vector2(engine.screen.size).y

                log("Mouse"+str((Vector2(event.position) * screen_ratio + get_level().screen_pos).get_tuple())+ " " + "Events" + str(
                    input_manager.get_mouse()[0].get_tuple()))
            elif type(event) is sfml.ResizeEvent:
                self.real_screen_size = Vector2(event.size)
                Engine.init_screen(self)

                #glViewport(0, 0, event.width, event.height)

