import sfml
from engine.const import log

from engine.snd_manager import SndManager


__author__ = 'Elias'


class SFMLSndManager(SndManager):
    def __init__(self):
        SndManager.__init__(self)
        self.fade_out_audio = []
        self.fade_in_audio = []

    def add_music_to_playlist(self,new_playlist):
        pass

    def set_playlist(self,music_list):
        self.playlist = music_list
        self.fade_out(self.music)


        try:
            self.music = sfml.Music.from_file(self.playlist[0])
            self.fade_in(self.music)
            self.music.play()
        except IOError:
            pass
        except IndexError:
            print("Playlist empty")
    def update_music_status(self):
        """log("IN")
        log(self.fade_in_audio)
        log("OUT")
        log(self.fade_out_audio)"""
        if self.music is not None:
            if self.music.status == sfml.Music.STOPPED:
                status = True
                self.music_index = (self.music_index + 1)
                if self.music_index == len(self.playlist):
                    self.music_index = len(self.playlist)-1
                try:
                    self.music = sfml.Music.from_file(self.playlist[self.music_index])
                    self.music.play()
                except IOError:
                    pass
                except IndexError:
                    pass

        delete_sounds = []
        for s in self.sounds_playing:
            if s.status == sfml.Sound.STOPPED:
                delete_sounds.append(s)
        for music in self.fade_out_audio:
            if music.volume == 0:
                delete_sounds.append(music)
            if music == self.music:
                del music
        for s in delete_sounds:
            self.sounds_playing.remove(s)
            try:
                self.fade_out_audio.remove(s)
            except ValueError:
                pass
            try:
                self.fade_in_audio.remove(s)
            except ValueError:
                pass
        del delete_sounds[:]

        tmp_out = []
        for audio in self.fade_out_audio:
            if audio.volume > 0:
                audio.volume -= 0.75
                if audio.volume <= 0:
                    audio.volume = 0
                    tmp_out.append(audio)
        for s in tmp_out:
            try:
                self.fade_out_audio.remove(s)
            except ValueError:
                pass

        tmp_in = []
        for audio in self.fade_in_audio:
            if audio.volume >= 0:
                audio.volume += 3
                if audio.volume >= 100:
                    audio.volume = 100
                    tmp_in.append(audio)
        for s in tmp_in:
            try:
                self.fade_in_audio.remove(s)
            except ValueError:
                pass
    def load_sound(self, name, permanent=False):
        """Load a sound in the system and returns it"""
        try:
            self.sounds[name]
        except KeyError:
            try:
                self.sounds[name] = sfml.SoundBuffer.from_file(name)
            except IOError:
                log("Error: Importing "+name,1)
                return None
        if permanent:
            self.permanent_sound.append(name)
        return self.sounds[name]

    def get_music_status(self):
        return self.music.status == sfml.Music.STOPPED

    def fade_out(self, audio):
        if audio is not None:
            audio.volume = 100
            self.fade_out_audio.append(audio)

    def fade_in(self, audio):
        if audio is not None:
            audio.volume = 0
            self.fade_in_audio.append(audio)

    def play_sound(self, sound):
        """
        Plays a given sound
        """
        if sound is not None:
            sound_playing = sfml.Sound(sound)
            sound_playing.play()
            self.sounds_playing.append(sound_playing)
            return sound_playing
        else:
            return None
    def stop_sound(self,sound):
        if sound is not None:
            sound.stop()
    def sound_status(self, sound_playing):
        if sound_playing is not None:
            return sound_playing.status == sfml.Sound.STOPPED
        else:
            return True