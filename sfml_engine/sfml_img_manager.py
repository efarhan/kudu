import sfml
from engine.const import log

try:
    import cv
    import numpy as np
except ImportError:
    log("Video module will not be available")
from OpenGL.GL import *
from OpenGL.GLU import *

from engine.rect import Rect
from engine.img_manager import ImgManager, BLEND_MODE, MASK_TYPE
from engine.init import engine
from engine.vector import Vector2

__author__ = 'Elias'


class SFMLImgManager(ImgManager):
    def __init__(self):
        ImgManager.__init__(self)

    def clear_screen(self, screen):
        screen.clear()

    @staticmethod
    def cv2array(im):
        depth2dtype = {
            cv.IPL_DEPTH_8U: 'uint8',
            cv.IPL_DEPTH_8S: 'int8',
            cv.IPL_DEPTH_16U: 'uint16',
            cv.IPL_DEPTH_16S: 'int16',
            cv.IPL_DEPTH_32S: 'int32',
            cv.IPL_DEPTH_32F: 'float32',
            cv.IPL_DEPTH_64F: 'float64',
        }

        arrdtype = im.depth
        a = np.fromstring(
            im.tostring(),
            dtype=depth2dtype[im.depth],
            count=im.width * im.height * im.nChannels)
        a.shape = (im.height, im.width, im.nChannels)
        return a
    def set_image(self, capture):
        #capture next frame
        if capture is not None:
            image = cv.QueryFrame(capture)
            if image is None:
                capture = None
                if self.vid_loop:
                    self.init_video()
                    image = cv.QueryFrame(capture)
                else:
                    return
            image_size = cv.GetSize(image)

            cv.Flip(image, None, 0)
            #cv.Flip(image, None, 1)
            cv.CvtColor(image, image, cv.CV_BGR2RGB)
            #you must convert the image to array for glTexImage2D to work
            #maybe there is a faster way that I don't know about yet...
            image_arr = self.cv2array(image)
            #print image_arr


            # Create Texture
            glTexImage2D(GL_TEXTURE_2D,
                         0,
                         GL_RGB,
                         image_size[0],
                         image_size[1],
                         0,
                         GL_RGB,
                         GL_UNSIGNED_BYTE,
                         image_arr)

    def display(self, capture):
        origin_pos = engine.get_origin_pos()
        video_size = engine.real_screen_size-origin_pos
        print origin_pos, video_size

        if capture is not None:
            #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glEnable(GL_TEXTURE_2D)
            #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
            #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
            #glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
            #this one is necessary with texture2d for some reason
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

            # Set Projection Matrix
            glMatrixMode(GL_PROJECTION)
            glLoadIdentity()
            gluOrtho2D(0, engine.real_screen_size.x, 0, engine.real_screen_size.y)

            # Switch to Model View Matrix
            glMatrixMode(GL_MODELVIEW)
            glLoadIdentity()

            # Draw textured Quads
            glBegin(GL_QUADS)
            glTexCoord2f(0.0, 0.0)
            glVertex2f(float(origin_pos.x), float(origin_pos.y))
            glTexCoord2f(1.0, 0.0)
            glVertex2f(float(video_size.x), float(origin_pos.y))
            glTexCoord2f(1.0, 1.0)
            glVertex2f(float(video_size.x), float(video_size.y))
            glTexCoord2f(0.0, 1.0)
            glVertex2f(float(origin_pos.x), float(video_size.y))
            glEnd()
    def init_video(self, name):
        return cv.CaptureFromFile(name)

    def show_video(self, video, screen):
        screen.pop_GL_states()
        self.set_image(video)
        self.display(video)
        screen.push_GL_states()
    @staticmethod
    def get_size(image):
        return Vector2(image.texture.size)

    def load_image(self, name, tmp=False):
        log("Loading image: "+name)
        try:
            self.img_name[name]
        except KeyError:
            try:
                log("Load sfml texture: "+name)
                self.img_name[name] = sfml.Texture.from_file(name)
            except IOError as e:
                log(str(e), 1)
                return None
            if tmp:
                self.tmp_images.append(name)

        return sfml.Sprite(self.img_name[name])

    def show_image(self, image, screen, pos, angle=0, center=False,
                   new_size=None, center_image=False,flip=False):
        diff_pos = Vector2()
        if image is None or new_size is None:
            return

        """Check position topleft and bottomright"""
        topleft_pos = (Vector2()+pos)
        bottomright_pos = (pos+new_size)

        if topleft_pos.x < 0:
            topleft_pos.x = 0
        if topleft_pos.y < 0:
            topleft_pos.y = 0

        if bottomright_pos.x > engine.screen_size.x:
            bottomright_pos.x = engine.screen_size.x
        if bottomright_pos.y > engine.screen_size.y:
            bottomright_pos.y = engine.screen_size.y

        if topleft_pos.x >= bottomright_pos.x or topleft_pos.y >= bottomright_pos.y:
            return

        if topleft_pos.x < 0 and bottomright_pos.x < 0:
            return
        if topleft_pos.x > engine.screen_size.x and bottomright_pos.x > engine.screen_size.x:
            return
        if topleft_pos.y < 0 and bottomright_pos.y < 0:
            return
        if topleft_pos.y > engine.screen_size.y and bottomright_pos.y > engine.screen_size.y:
            return

        try:
            sprite = image
            origin_pos = engine.origin_pos
            screen_diff_ratio = engine.ratio

            if new_size:
                text_size = None

                if isinstance(sprite, sfml.Sprite):
                    text_size = Vector2(sprite.texture.size)
                else:
                    text_size = new_size
                sprite.ratio = (new_size * screen_diff_ratio / text_size).get_tuple()
                size_ratio = (text_size/new_size)
                sub_rect = Rect(((topleft_pos-pos)*size_ratio),
                                ((bottomright_pos-topleft_pos)*size_ratio))



            if angle != 0:

                new_angle = angle % 360
                if new_angle > 180:
                    new_angle -= 360
                v = new_size/2
                v.rotate(new_angle)
                diff_pos = Vector2() - v + new_size/2
                sprite.rotation = angle

            if isinstance(sprite, sfml.Sprite):

                if flip:
                    sprite.texture_rectangle = (
                        sfml.Rectangle(sfml.Vector2(sprite.texture.size[0], 0),
                                       sfml.Vector2(-sprite.texture.size[0], sprite.texture.size[1])))
                    sprite.position = (origin_pos + (pos + diff_pos) * screen_diff_ratio).get_tuple()
                else:
                    sprite.texture_rectangle = \
                    sfml.Rectangle(sub_rect.pos.get_int_tuple(),
                                   sub_rect.size.get_int_tuple())
                    sprite.position = (origin_pos + (topleft_pos + diff_pos) * screen_diff_ratio).get_tuple()
            elif isinstance(sprite, sfml.Text):
                sprite.position = (origin_pos + (pos + diff_pos) * screen_diff_ratio).get_tuple()
            screen.draw(sprite)

        except KeyError:
            pass

    def draw_rect(self, screen,  rect, color=(255,255,255,255), angle=0):
        drawing_rect = sfml.RectangleShape()
        origin_pos = engine.origin_pos
        screen_diff_ratio = engine.ratio

        diff_pos = Vector2()
        if angle != 0:
            new_angle = angle % 360
            if new_angle > 180:
                new_angle -= 360
            v = rect.size/2
            v.rotate(new_angle)
            diff_pos = Vector2() - v + rect.size/2

            drawing_rect.rotation = angle
        new_pos = (origin_pos + (rect.pos + diff_pos) * screen_diff_ratio)
        drawing_rect.position = sfml.Vector2(new_pos.x, new_pos.y)
        new_size = (rect.size * screen_diff_ratio)
        drawing_rect.size = sfml.Vector2(new_size.x, new_size.y)
        log(str(new_pos)+" "+str(new_size)+" "+str(color))
        if len(color) == 4:
            drawing_rect.fill_color = sfml.Color(color[0], color[1], color[2], color[3])
        elif len(color) == 3:
            drawing_rect.fill_color = sfml.Color(color[0], color[1], color[2])
        screen.draw(drawing_rect)

    def show_mask_img(self,
                      screen,
                      bg,
                      mask,
                      bg_rect,
                      mask_rect,
                      bg_angle=0,
                      mask_angle=0,
                      blend_mode=BLEND_MODE.MULTIPLY,
                      mask_type=MASK_TYPE.RECT):
        alpha_states = None
        if blend_mode == BLEND_MODE.ADD:
            alpha_states = sfml.RenderStates(sfml.BlendMode.BLEND_ADD)
        elif blend_mode == BLEND_MODE.MULTIPLY:
            alpha_states = sfml.RenderStates(sfml.BlendMode.BLEND_MULTIPLY)
        elif blend_mode == BLEND_MODE.ALPHA:
            alpha_states = sfml.RenderStates(sfml.BlendMode.BLEND_ALPHA)


        if mask_type == MASK_TYPE.RECT:
            pass
        elif mask_type == MASK_TYPE.CIRCLE:
            pass
        elif mask_type == MASK_TYPE.TEXTURE:
            pass


        mask_texture_size = Vector2(mask.img.texture.size)

        origin_pos = engine.get_origin_pos()
        screen_diff_ratio = engine.get_ratio()


        mask_pos = (origin_pos + (mask_rect.pos) * screen_diff_ratio)
        mask_size = mask_rect.size * screen_diff_ratio
        mask_render = sfml.RenderTexture(mask_size.x, mask_size.y)

        mask_render.clear(sfml.Color(0, 0, 0, 0))
        mask.img.ratio = (mask_size/mask_texture_size).get_list()
        mask.img.position = [0,0]

        bg_pos = (origin_pos + (bg_rect.pos) * screen_diff_ratio)
        bg_size = bg_rect.size * screen_diff_ratio

        bg_texture_size = Vector2(bg.img.texture.size)
        bg.img.ratio = (bg_size/bg_texture_size).get_list()
        bg.img.position = (bg_pos-mask_pos).get_list()
        if bg_angle != 0:
            bg.img.rotation = bg_angle
        mask_render.draw(bg.img)
        if mask_angle != 0:
            mask.img.rotation = mask_angle

        mask_render.draw(mask.img, states=alpha_states)
        mask_render.display()
        mask_render_sprite = sfml.Sprite(mask_render.texture)
        mask_render_sprite.position = mask_pos.get_list()
        screen.draw(mask_render_sprite)
    def sanitize_img_manager(self, delete_images=[],remove_all=False):
        pass

