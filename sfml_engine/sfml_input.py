import sfml

from engine.const import log, enum
from engine.vector import Vector2
from engine.input_manager import InputManager


__author__ = 'efarhan'


keycodes = {
    "up": sfml.Keyboard.UP,
    "down": sfml.Keyboard.DOWN,
    "left": sfml.Keyboard.LEFT,
    "right": sfml.Keyboard.RIGHT,
    "escape": sfml.Keyboard.ESCAPE,
    "LCTRL": sfml.Keyboard.L_CONTROL,
    "RCTRL": sfml.Keyboard.R_CONTROL,
    "enter": sfml.Keyboard.RETURN,
    "backspace": sfml.Keyboard.BACK_SPACE,
    "escape": sfml.Keyboard.ESCAPE,
    "tab": sfml.Keyboard.TAB,
    "LALT": sfml.Keyboard.L_ALT,
    "RALT": sfml.Keyboard.R_ALT,
    "LSYS": sfml.Keyboard.L_SYSTEM,
    "RSYS": sfml.Keyboard.R_SYSTEM,
    "LSHIFT": sfml.Keyboard.L_SHIFT,
    "RSHIFT": sfml.Keyboard.R_SHIFT,
    "pad0": sfml.Keyboard.NUMPAD0,
    "a": sfml.Keyboard.A,
    "0": sfml.Keyboard.NUM0,
    "f1": sfml.Keyboard.F1

}

AXIS_TYPE = enum('TRIGGER', 'STICK', 'HAT')

class SFMLInput(InputManager):
    """SFML Input Class"""

    def init_joystick(self):
        log("Init joystick")
        InputManager.init_joystick(self)
        self.axis_definition = {}
        for joy in range(sfml.Joystick.COUNT):
            if sfml.Joystick.is_connected(joy):
                for i in range(sfml.Joystick.AXIS_COUNT):
                    if sfml.Joystick.has_axis(joy,i):
                        if i < 6:
                            if sfml.Joystick.get_axis_position(joy, i) < -75:
                                self.axis_definition['JOY'+str(joy)+'AXIS'+str(i)] = AXIS_TYPE.TRIGGER
                            else:
                                self.axis_definition['JOY'+str(joy)+'AXIS'+str(i)] = AXIS_TYPE.STICK
        self.update_joy_event()

    def init_keyboard(self):
        InputManager.init_keyboard(self)

        for key in keycodes.keys():
            self.button_value[key] = 0
            self.button_key[keycodes[key]] = key
        for key in range(ord('z')-ord('a')+1):
            self.button_value[chr(ord('a')+key)] = 0
            self.button_key[keycodes['a']+key] = chr(ord('a')+key)
        for key in range(ord('9')-ord('0')+1):
            self.button_value[chr(ord('0')+key)] = 0
            self.button_key[keycodes['0']+key] = chr(ord('0')+key)
        for key in range(ord('9')-ord('0')+1):
            self.button_value["pad"+chr(ord('0')+key)] = 0
            self.button_key[keycodes['pad0']+key] = "pad"+chr(ord('0')+key)
        for key in range(1,12+1):
            self.button_value["f"+str(key)] = 0
            self.button_key[keycodes['f1']+key] = "f"+str(key)





    def update_keyboard_event(self,event=None):
        '''
        Update the states of Input Event
        '''
        if type(event) == sfml.KeyEvent:
            try:
                self.button_value[self.button_key[event.code]] = event.pressed
            except KeyError:
                '''Key not in map'''
                log("Key not mapped: "+str(event.code), 1)



    def get_current_axis(self):
        current_joys = []
        for axis in self.axis.keys():
            try:

                if self.axis_definition[axis] == AXIS_TYPE.TRIGGER:
                    trigger_command = "JOY_%s_AXIS_%s_"%(axis[3],axis[8])
                    if self.axis[axis] > 50:

                        current_joys.append(trigger_command+"+")
                elif self.axis_definition[axis] == AXIS_TYPE.STICK:
                    trigger_command = "JOY_%s_AXIS_%s_"%(axis[3],axis[8])
                    if self.axis[axis] > 50:
                        current_joys.append(trigger_command+"+")
                    elif self.axis[axis] < -50:
                        current_joys.append(trigger_command+"-")
            except KeyError:
                log("Error while getting: "+axis,1)
        return current_joys



    def add_one_key(self, key_value):
        """
        Add only one key
        """
        self.button_value[key_value] = 0
        try:
            if ord('a') <= ord(key_value) <= ord('z'):
                self.button_key[ord(key_value) - ord('a') + keycodes['a']] = key_value
            elif ord('0') <= ord(key_value) <= ord('9'):
                self.button_key[ord(key_value) - ord('0') + keycodes['0']] = key_value
        except TypeError:
            '''the key value is not a letter or a number'''
            try:
                self.button_key[keycodes[key_value]] = key_value
            except IndexError:
                pass
            except KeyError:
                pass

    def add_key_button(self,action,key_list):
        self.button_map[action] = key_list
        for key_value in key_list:
            keys = key_value.split("+")
            for k in keys:
                self.add_one_key(k)
    """MOUSE"""

    def get_mouse(self):
        """
        Return mouse state as
        position, (left, right,middle)
        """
        from engine.init import engine

        mouse_pos = Vector2(sfml.Mouse.get_position(engine.screen))
        mouse_pos = (mouse_pos - engine.get_origin_pos())*(engine.screen_size.y/(engine.real_screen_size.y-2*engine.get_origin_pos().y))
        mouse_buttons = [sfml.Mouse.is_button_pressed(sfml.Mouse.LEFT),
                        sfml.Mouse.is_button_pressed(sfml.Mouse.RIGHT),
                        sfml.Mouse.is_button_pressed(sfml.Mouse.MIDDLE)]
        return mouse_pos, mouse_buttons

    def show_mouse(show=True):
        """
        Show/hide mouse
        """
        from engine.init import engine
        engine.screen.mouse_cursor_visible = show

    """JOYSTICK"""

    def update_joy_event(self):
        for joy in range(sfml.Joystick.COUNT):
            if sfml.Joystick.is_connected(joy):
                for i in range(sfml.Joystick.AXIS_COUNT):
                    if sfml.Joystick.has_axis(joy,i):
                        if i < 6:
                            self.axis['JOY'+str(joy)+'AXIS'+str(i)] = sfml.Joystick.get_axis_position(joy, i)
                        else:
                            self.axis['JOY'+str(joy)+'HAT'+str(i-6)] = sfml.Joystick.get_axis_position(joy, i)
                for i in range(sfml.Joystick.get_button_count(joy)):
                    self.button_value['JOY'+str(joy)+'BUTTON'+str(i)] = sfml.Joystick.is_button_pressed(joy, i)

    def get_joy_button(self,action):

        try:
            button_key_list = self.button_map[action]
            value = False
            for button_key in button_key_list:
                if 'BUTTON' in button_key:

                    value = value or self.button_value["".join(button_key.split('_'))]
                elif 'AXIS' in button_key:
                    parameters = button_key.split('_')
                    axis_name = "".join(parameters[0:len(parameters)-1])

                    if parameters[-1] == '-':
                        value = value or self.axis[axis_name] < -50
                    elif parameters[-1] == '+':
                        value = value or self.axis[axis_name] > 50
                elif 'HAT' in button_key:
                    pass
            return value
        except KeyError:
            pass

        return False

    def add_joy_button(self,action, button_list):
        self.button_map[action] = button_list