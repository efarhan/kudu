import time
from engine.img_manager import img_manager
from engine.rect import Rect
from engine.snd_manager import snd_manager
from engine.vector import Vector2
from engine.scene import Scene
from engine.init import engine
from game_object.image import Image
from menu.menustate import MenuState


class Kwakwa(Scene):
    def init(self):
        self.text = Image(path='data/sprites/text/kwakwa.png',
                          pos=engine.screen_size/2,
                          size=Vector2(320,215))
        self.text.init_image()
        self.text.pos = self.text.pos-self.text.size/2


        self.screen_pos = Vector2()
        self.first = False
        snd_manager.play_sound(snd_manager.load_sound("data/sound/pissed_off_duck.ogg"))
        self.begin = time.time()
        self.end = 2.962
    def loop(self, screen):
        snd_manager.update_music_status()
        img_manager.draw_rect(screen=screen,rect=Rect(pos=Vector2(),size=engine.get_screen_size()),color=(255,255,255))
        self.text.loop(screen)
        if time.time()-self.begin > self.end:
            from engine.level_manager import switch_level
            snd_manager.set_playlist([])
            switch_level(MenuState())


