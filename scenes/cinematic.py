from engine.img_manager import img_manager
from engine.rect import Rect
from engine.vector import Vector2
from engine.input_manager import input_manager

try:
    import gc
    from engine.init import engine
    from engine.const import CONST
    from engine.const import log
    from engine.scene import Scene
    from engine.snd_manager import snd_manager
    from game_object.video import Video
except ImportError as e:
    log(e,1)

__author__ = 'Elias'


class Cinematic(Scene):
    def __init__(self):
        Scene.__init__(self)

    def init(self, loading=False):
        Scene.init(self)
        #self.sound = snd_manager.load_sound("data/sound/introb.ogg")
        self.video = Video(path="data/video/video.webm")
        self.video.init()

        #snd_manager.play_sound(self.sound)
    def loop(self, screen):
        Scene.loop(self,screen)
        img_manager.draw_rect(screen, Rect(Vector2(),engine.screen_size), color=(0,0,0))
        status = self.video.loop(screen)
        snd_manager.update_music_status()
        skip = input_manager.get_button("first_action")
        if skip:
            status = True
        if status:
            self.video.exit()
            from engine import level_manager
            from menu.menustate import MenuState
            engine.set_framerate(CONST.framerate)
            level_manager.switch_level(MenuState())