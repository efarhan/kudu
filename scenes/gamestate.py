"""
Created on 9 dec. 2013

@author: efarhan
"""
from engine import level_manager
from engine.img_manager import img_manager
from engine.init import engine
from engine.input_manager import input_manager
from engine.physics_manager import physics_manager
from engine.rect import Rect
from engine.snd_manager import snd_manager
from engine.vector import Vector2
from engine.scene import Scene
from engine.const import log, CONST
from json_export.level_json import load_level
from editor.editor import Editor
from scenes.gui import GUI
from menu.menustate import MenuState


class GameState(Scene, Editor, GUI):
    def __init__(self, filename):
        self.bg_color = [0, 0, 0]
        self.player = None
        self.event = {}
        self.filename = filename
        if CONST.debug:
            Editor.__init__(self)
        GUI.__init__(self)

    def init(self, loading=False):

        physics_manager.init_world()
        self.objects = [ [] for i in range(CONST.layers) ]
        self.screen_pos = Vector2()
        self.show_mouse = False
        if self.filename != "":
            log("Loading level " + self.filename)
            if not load_level(self):
                from engine.level_manager import switch_level
                switch_level(Scene())
        self.lock = False #TODO: lock animation
        self.click = False


        if not loading:
            log("EXECUTE INIT EVENT")
            self.execute_event('on_init')
        log("GAMESTATE INIT OVER")

    def execute_event(self, name):
        try:
            if self.event[name]:
                self.event[name].execute()
        except KeyError as e:
            log("Error: No such event: %s"%(name)+str(e), 1)

    def reload(self, newfilename):
        self.filename = newfilename
        self.init()

    def loop(self, screen):
        Scene.loop(self,screen)
        img_manager.draw_rect(screen, rect=Rect(Vector2(),size=engine.screen_size),color=self.bg_color)
        snd_manager.update_music_status()
        """
        if CONST.render == 'kivy':
            for layer in self.objects:
                for img in layer:
                    #set img pos outside the screen
                    if isinstance(img, AnimImage):
                        for kivy_img in img.anim.img_indexes:
                            kivy_img.x = -engine.get_screen_size().x
                            kivy_img.y = -engine.get_screen_size().y
        """
        '''Event
        If mouse_click on element, execute its event, of not null'''
        if self.show_mouse:
            input_manager.show_mouse()
            mouse_pos, pressed = input_manager.get_mouse()
            if pressed[0] and not self.click:
                event = None
                self.click = True
                for layer in self.objects:
                    for image in layer:
                        if image.check_click(mouse_pos, self.screen_pos):
                            event = image.event
                if event:
                    event.execute()
            elif not pressed[0]:
                self.click = False



        if not self.lock:
            physics_manager.loop(dt=engine.get_framerate())


        '''Show images'''

        for i, layer in enumerate(self.objects):
            remove_image = []
            for j, img in enumerate(layer):
                img.loop(screen)
                if img.remove:
                    remove_image.append(img)
            for r in remove_image:
                if r.body:
                    physics_manager.remove_body(r.body)
                self.objects[i].remove(r)

        '''GUI'''
        GUI.loop(self, screen)

        '''Editor'''
        if CONST.debug:
            Editor.loop(self, screen, self.screen_pos)

        if input_manager.get_button('start_menu'):
            #TODO: pause menu
            self.exit()
            level_manager.switch_level(MenuState())
        #log(engine.get_framerate())
    def exit(self):
        physics_manager.remove_world(physics_manager.current_world)

        Scene.exit(self)
