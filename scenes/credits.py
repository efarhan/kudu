from engine import level_manager
from engine.const import CONST
from engine.init import engine
from engine.scene import Scene
from engine.vector import Vector2
from game_object.text import Text

__author__ = 'Elias'


class Credits(Scene):
    def __init__(self):
        Scene.__init__(self)
        texts = [
            "Power Creep",
            "",
            "a game by Team-KwaKwa",
            "",
            "Lead Designer",
            "Elias Farhan",
            "",
            "Artists",
            "Dan Iorgulescu",
            "Andre Terramorsi",
            "Virgile Paultre",
            "",
            "Music & SFX",
            "Dorian Sred",

        ]

        self.texts =[]
        size = 150
        for i,t in enumerate(texts):
            self.texts.append(Text(pos=Vector2(engine.screen_size.x/2,engine.screen_size.y+i*size),
                                   size=size,
                                   text=t,
                                   center=True,
                                   font=CONST.font,
                                   color=(255,255,255)))
        self.chrono = 0
        self.speed = 3
        self.screen_pos = Vector2()
        #snd_manager.set_playlist(["data/music/Boss Theme.ogg"])

    def loop(self, screen):

        self.chrono += 1
        for t in self.texts:
            t.loop(screen)
            t.pos.y -= self.speed


        if self.texts[-1].pos.y < -self.texts[-1].size.y:
            from menu.menustate import MenuState
            level_manager.switch_level(MenuState())