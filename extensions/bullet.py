from engine.init import engine
from engine.level_manager import get_level
from engine.rect import Rect
from engine.vector import Vector2
from game_object.image import Image

__author__ = 'Elias'


class Bullet(Image):
    def __init__(self,pos,size=None,angle=0,relative=False,path="", speed=Vector2()):
        Image.__init__(self, pos=pos, size=size,angle=angle,relative=False,path=path)
        self.speed = speed
    def loop(self, screen):
        if not self.rect.collide_rect(Rect(get_level().screen_pos,engine.screen_size)):
            self.remove = True
        Image.loop(self,screen)

    @staticmethod
    def parse_image(image_data, pos, size, angle):

        return Bullet()