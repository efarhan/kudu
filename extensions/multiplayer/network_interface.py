import time
import threading
from engine import stat
from engine.const import log
from extensions.packet_descriptor import PacketDescriptor
import json
from json_export import json_main
from network import pynetwork_manager

found_game = False


class ServerRespone():
    def __init__(self, response_json):
        try:
            pass
        except KeyError:
            pass

    @staticmethod
    def parse_message(response_json):
        pass


def parse_message(message):
    log(message)
    d = None
    try:
        d = json.loads(message)
    except ValueError:
        log("Error while loading JSON: "+message,1)
        return False
    status = packet_status_succeeded(d)
    return status


def parse_dict(dict):
    if is_status_feedback(dict):
        return PacketDescriptor("server", "success" if packet_status_succeeded(dict) else "failure", dict)
    else:
        return PacketDescriptor(dict["value"]["id"], dict["action"], dict["value"])


def packet_status_succeeded(dict):
    status = json_main.get_element(dict, "status")
    if status is None:
        log("Message has no status and thus cannot be checked for success of failure", 1)
        return False
    return dict["status"] == "success"

def authentificate(name=""):
    name_command = stat.get_value("pseudo")
    if name is not "":
        name_command = name

    if name_command is "" or name_command is None:
        name_command = "Player"

    id_command = pynetwork_manager.self_id

    command_json = {
        "timestamp": int(time.time()*1000),
        "action": 'authenticate:user',
        "value": {"id": str(id_command), "name": name_command}
    }

    command = json.dumps(command_json)
    pynetwork_manager.send_data(command)
    status = parse_message(pynetwork_manager.recv_data())
    log(status)
    return status


def loading_search():
    global found_game
    answer = pynetwork_manager.recv_data()
    log("Answer len: "+str(len(answer)))
    found_game = parse_message(answer)


def search_game():
    command_json = {
        "timestamp": int(time.time()*1000),
        "action": 'initiate:search:game'
    }
    command = json.dumps(command_json)
    pynetwork_manager.send_data(command)

    loading_thread = threading.Thread(target=loading_search)
    loading_thread.daemon = True
    loading_thread.start()

def interrupt_search():
    command_json = {
        "timestamp": int(time.time()*1000),
        "action": 'interrupt:search:game'
    }
    command = json.dumps(command_json)
    pynetwork_manager.send_data(command)
    log(pynetwork_manager.recv_data())