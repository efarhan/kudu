import json
import os
import uuid
from animation.animation_main import Animation
from engine import stat
from engine.const import log, enum
from extensions.multiplayer import network_interface
from json_export import json_main
from network import pynetwork_manager

__author__ = 'Elias'


NETWORK_STATE = enum('NOT_CONNECTED',
                     'CONNECTED',
                     'AUTHENTIFIED',
                     'SEARCHING',
                     'GAMING')

class MultiplayerManager(Animation):
    def __init__(self,obj):
        Animation.__init__(self,obj)
        log("""SAVES MANAGEMENT""")
        if not os.path.exists('data/saves/saves.json'):
            pynetwork_manager.generate_id()
            save_dict = {
                "id": str(pynetwork_manager.self_id),
                "pseudo": "Player"
            }
            id_file = open('data/saves/saves.json', 'w')
            id_file.write(json.dumps(save_dict))
            id_file.close()
        else:
            try:
                id_file = json_main.load_json('data/saves/saves.json')
                if id_file is None:
                    raise IOError
                self_id = id_file["id"]
                stat.set_value("pseudo", id_file["pseudo"])
                pynetwork_manager.self_id = uuid.UUID(id_file["id"])
            except IOError:
                pynetwork_manager.generate_id()
                save_dict = {
                    "id": str(pynetwork_manager.self_id),
                    "pseudo": "Player"
                }
                stat.set_value("pseudo", "Player")
                id_file = open('data/saves/saves.json', 'w')
                id_file.write(json.dumps(save_dict))
                id_file.close()
        log("ID: "+str(pynetwork_manager.self_id))
        pynetwork_manager.connect()
        self.searching = False
        self.state = NETWORK_STATE.NOT_CONNECTED

    def load_images(self,size=None,tmp = False):
        pass

    def update_animation(self,state="",invert=False):
        self.update_state()
        return Animation.update_animation(self,state=state, invert=invert)

    def update_state(self):
        if pynetwork_manager.is_connect and self.state == NETWORK_STATE.NOT_CONNECTED:
            self.state = NETWORK_STATE.CONNECTED


        if pynetwork_manager.is_connect and self.state == NETWORK_STATE.CONNECTED:
            log("AUTH BEGIN")
            status = network_interface.authentificate()
            if status:
                self.state = NETWORK_STATE.AUTHENTIFIED
            log("AUTH END")

        if pynetwork_manager.is_connect and self.state == NETWORK_STATE.AUTHENTIFIED:
            log("SEARCH BEGIN")
            network_interface.search_game()
            self.state = NETWORK_STATE.SEARCHING

        if pynetwork_manager.is_connect and self.state == NETWORK_STATE.SEARCHING:
            if network_interface.found_game:
                log("SEARCH END")
                self.state = NETWORK_STATE.GAMING

        if not pynetwork_manager.is_connect:
            log("RECONNECT")
            self.state = NETWORK_STATE.NON_CONNECTED
            pynetwork_manager.connect()
    @staticmethod
    def parse_special_animation(anim, anim_data):
        pass